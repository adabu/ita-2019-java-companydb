package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.entity.Address;

/**
 * Contains methods for transformation between instances of {@code CompanyBranchDto} and {@code CompanyBranch}.
 */
public interface AddressTransformer extends Transformer<AddressDto, Address> {
}
