package com.ita2019java.usersapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProjectController.class)
class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ProjectService projectService;

    @Test
    void getReturnsAllProjects() throws Exception {
        final ProjectDto project = newProjectDto();
        when(projectService.findAll()).thenReturn(List.of(project));

        mockMvc.perform(get("/project"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(project.getId()));

        verify(projectService).findAll();
    }

    @Test
    void getWithNameAndCreationTimestampReturnsAllProjects() throws Exception {
        final ProjectDto project = newProjectDto();
        final String nameParam = "substring";
        final LocalDateTime afterParam = LocalDateTime.now();
        when(projectService.findAll(nameParam, afterParam)).thenReturn(List.of(project));

        mockMvc.perform(
                get("/project")
                        .param("name", nameParam)
                        .param("after", String.valueOf(afterParam)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(project.getId()));

        verify(projectService).findAll(eq(nameParam), eq(afterParam));
    }

    @Test
    void createDelegatesCreationToService() throws Exception {
        final ProjectDto projectDto = newProjectDto();
        mockMvc.perform(
                post("/project")
                        .content(objectMapper.writeValueAsString(projectDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(projectService).create(any(ProjectDto.class));
    }

    @Test
    void updateDelegatesUpdateToService() throws Exception {
        final ProjectDto projectDto = newProjectDto();
        mockMvc.perform(
                put("/project")
                        .content(objectMapper.writeValueAsString(projectDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(projectService).update(any(ProjectDto.class));
    }

    @Test
    void deleteDelegatesDeleteToService() throws Exception {
        final long id = 1L;
        mockMvc.perform(
                delete("/project")
                        .param("id", String.valueOf(id)))
                .andExpect(status().is2xxSuccessful());
        verify(projectService).delete(eq(id));
    }

    private ProjectDto newProjectDto() {
        final CompanyDto companyDto = CompanyDto.builder()
                .setId(1L).build();

        return ProjectDto.builder()
                .setId(2L)
                .setName("Name")
                .setCompany(companyDto)
                .build();
    }

}
