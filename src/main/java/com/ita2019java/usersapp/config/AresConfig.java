package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.ares.util.CompanyExtractor;
import com.ita2019java.usersapp.service.AresService;
import com.ita2019java.usersapp.service.AresServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class AresConfig {

    @Bean
    public AresService aresService(@Value("${ares.user.mail}") String userMail,
                                   Jaxb2Marshaller aresRequestMarshaller, Jaxb2Marshaller aresAnswerMarshaller) {
        AresServiceImpl aresService = new AresServiceImpl(userMail);
        aresService.setMarshaller(aresRequestMarshaller);
        aresService.setUnmarshaller(aresAnswerMarshaller);
        return aresService;
    }

    @Bean
    public Jaxb2Marshaller aresRequestMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("cz.mfcr.ares.request");
        return marshaller;
    }

    @Bean
    public Jaxb2Marshaller aresAnswerMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("cz.mfcr.ares.answer");
        return marshaller;
    }

    @Bean
    public CompanyExtractor companyExtractor() {
        return new CompanyExtractor();
    }

}
