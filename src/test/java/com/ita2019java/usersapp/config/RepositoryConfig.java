package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.repository.util.SchemaUtils;
import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryConfig {

    @Bean
    public SchemaUtils schemaUtils(Flyway flyway) {
        return new SchemaUtils(flyway);
    }

}
