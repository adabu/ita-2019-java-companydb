package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Project;

import java.time.LocalDateTime;
import java.util.List;

/**
 * A repository for storing and managing {@code Project} objects.
 */
public interface ProjectRepository {

    /**
     * Returns all projects stored in the repository.
     * @return a list containing all projects
     */
    List<Project> findAll();

    /**
     * Returns all projects stored in the repository that satisfy the conditions.
     * @param name a substring to be contained in project's name
     * @param createdAfter only projects created after {@code createdAfter} may be added to the resulting list
     * @return a list containing projects with substring {@code name} in their names and created after {@code createdAfter}
     */
    List<Project> findAll(String name, LocalDateTime createdAfter);

    /**
     * Creates new project in repository.
     * @param project the project to be created
     */
    void create(Project project);

    /**
     * Updates existing project in repository.
     * @param project the project to be updated
     */
    void update(Project project);

    /**
     * Deletes project from the repository.
     * @param project the project to be deleted
     */
    void delete(Project project);

    /**
     * Deletes project from the repository.
     * @param id an id of the project to be deleted
     */
    void deleteById(Long id);

}
