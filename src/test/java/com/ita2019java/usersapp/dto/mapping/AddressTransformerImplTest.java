package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.entity.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class AddressTransformerImplTest {

    private AddressTransformer addressTransformer;

    @BeforeEach
    void beforeEach() {
        addressTransformer = new AddressTransformerImpl();
    }

    @Test
    void transformationFromEntityToDtoReturnsCorrectDto() {
        final String street = "Some street";
        final String houseNumber = "100/1";
        final String city = "Some city";
        final String zipCode = "100 00";
        final String country = "Some country";

        Address address = new Address(street, houseNumber, city, zipCode, country);

        AddressDto addressDto = addressTransformer.toDto(address);

        final Object[] expectedPropertyValues = new Object[]{street, houseNumber, city, zipCode, country};
        final Object[] addressDtoPropertyValues = new Object[]{addressDto.getStreet(), addressDto.getHouseNumber(),
                addressDto.getCity(), addressDto.getZipCode(), addressDto.getCountry()};
        assertArrayEquals(expectedPropertyValues, addressDtoPropertyValues);
    }

    @Test
    void transformationFromEntityToDtoWithNullValuesReturnsDtoWithNulls() {
        Address address = new Address(null, null, null, null, null);

        AddressDto addressDto = addressTransformer.toDto(address);

        final Object[] addressDtoPropertyValues = new Object[]{addressDto.getStreet(), addressDto.getHouseNumber(),
                addressDto.getCity(), addressDto.getZipCode(), addressDto.getCountry()};
        assertThat(addressDtoPropertyValues).containsOnlyNulls();
    }

    @Test
    void transformationFromDtoToEntityReturnsCorrectDto() {
        final String street = "Some street";
        final String houseNumber = "100/1";
        final String city = "Some city";
        final String zipCode = "100 00";
        final String country = "Some country";

        AddressDto addressDto = new AddressDto(street, houseNumber, city, zipCode, country);

        Address address = addressTransformer.toEntity(addressDto);

        final Object[] expectedPropertyValues = new Object[]{street, houseNumber, city, zipCode, country};
        final Object[] addressPropertyValues = new Object[]{address.getStreet(), address.getHouseNumber(),
                address.getCity(), address.getZipCode(), address.getCountry()};
        assertArrayEquals(expectedPropertyValues, addressPropertyValues);
    }

    @Test
    void transformationFromDtoToEntityWithNullValuesReturnsDtoWithNulls() {
        AddressDto addressDto = new AddressDto(null, null, null, null, null);

        Address address = addressTransformer.toEntity(addressDto);

        final Object[] addressPropertyValues = new Object[]{address.getStreet(), address.getHouseNumber(),
                address.getCity(), address.getZipCode(), address.getCountry()};
        assertThat(addressPropertyValues).containsOnlyNulls();
    }

}
