package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.ProjectDto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * A service for working with {@code ProjectDto} objects.
 */
public interface ProjectService {

    /**
     * Returns all projects stored in project repository.
     * @return a list containing all projects
     */
    List<ProjectDto> findAll();

    /**
     * Returns all projects stored in project repository that satisfy the conditions.
     * @param name a substring to be contained in project's name
     * @param createdAfter only projects created after {@code createdAfter} may be added to the resulting list
     * @return a list containing projects with substring {@code name} in their names and created after {@code createdAfter}
     */
    List<ProjectDto> findAll(String name, LocalDateTime createdAfter);

    /**
     * Creates new project in project repository.
     * @param project the project to be created
     */
    void create(ProjectDto project);

    /**
     * Updates existing project in project repository.
     * @param project the project to be updated
     */
    void update(ProjectDto project);

    /**
     * Deletes project from project repository.
     * @param project the project to be deleted
     */
    void delete(ProjectDto project);

    /**
     * Deletes project from project repository.
     * @param id an id of the project
     */
    void delete(Long id);

}
