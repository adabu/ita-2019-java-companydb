DROP TABLE company;

CREATE TABLE company
(
    id                   bigint                 NOT NULL,
    created_at           timestamp without time zone,
    updated_at           timestamp without time zone,
    active               boolean                NOT NULL,
    company_id           character varying(8)   NOT NULL,
    address_city         character varying(30)  NOT NULL,
    address_country      character varying(3)   NOT NULL,
    address_house_number character varying(10)  NOT NULL,
    address_street       character varying(50)  NOT NULL,
    address_zip_code     character varying(6)   NOT NULL,
    name                 character varying(100) NOT NULL,
    note                 character varying(255),
    vat_id               character varying(10)  NOT NULL,
    web_url              character varying(255)
);

CREATE SEQUENCE company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE company_id_seq OWNED BY company.id;

ALTER TABLE ONLY company
    ALTER COLUMN id SET DEFAULT nextval('company_id_seq'::regclass);

ALTER TABLE ONLY company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
