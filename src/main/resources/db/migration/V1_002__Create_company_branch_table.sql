CREATE TABLE company_branch
(
    id                   bigint                 NOT NULL,
    created_at           timestamp without time zone,
    updated_at           timestamp without time zone,
    address_city         character varying(30)  NOT NULL,
    address_country      character varying(3)   NOT NULL,
    address_house_number character varying(10)  NOT NULL,
    address_street       character varying(50)  NOT NULL,
    address_zip_code     character varying(6)   NOT NULL,
    name                 character varying(100) NOT NULL,
    company_id           bigint                 NOT NULL
);

CREATE SEQUENCE company_branch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE company_branch_id_seq OWNED BY company_branch.id;

ALTER TABLE ONLY company_branch
    ALTER COLUMN id SET DEFAULT nextval('company_branch_id_seq'::regclass);

ALTER TABLE ONLY company_branch
    ADD CONSTRAINT company_branch_pkey PRIMARY KEY (id);

ALTER TABLE ONLY company_branch
    ADD CONSTRAINT company_branch_company_fk FOREIGN KEY (company_id) REFERENCES company (id);
