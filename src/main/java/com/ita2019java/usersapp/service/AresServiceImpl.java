package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.ares.AresDotazyFactory;
import cz.mfcr.ares.answer.AresOdpovedi;
import cz.mfcr.ares.request.AresDotazy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class AresServiceImpl extends WebServiceGatewaySupport implements AresService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AresServiceImpl.class);

    private final String userMail;

    public AresServiceImpl(String userMail) {
        this.userMail = userMail;
    }

    public AresOdpovedi getAresOdpovedi(AresDotazy aresDotazy) {
        LOGGER.info("Get ares odpovedi with \"ares dotazy\" of id '{}' called.", aresDotazy.getId());

        final String messageTarget = "http://wwwinfo.mfcr.cz/cgi-bin/ares/xar.cgi";
        AresOdpovedi aresOdpovedi = (AresOdpovedi) getWebServiceTemplate()
                .marshalSendAndReceive(messageTarget, aresDotazy);
        LOGGER.info("Requested \"ares odpovedi\" for \"ares dotazy\" with id {} " +
                "and received response with id {}.", aresDotazy.getId(), aresOdpovedi.getId());

        return aresOdpovedi;
    }

    public AresOdpovedi getAresOdpovediForCompanyId(String companyId) {
        LOGGER.info("Get ares odpovedi for company id {} called.", companyId);
        AresDotazy aresDotazy = AresDotazyFactory.forCompanyOfId(companyId, userMail);
        return getAresOdpovedi(aresDotazy);
    }

}
