CREATE TABLE project
(
    id         bigint                 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    end_date   character varying(255),
    name       character varying(100) NOT NULL,
    price      bigint,
    start_date character varying(20),
    company_id bigint                 NOT NULL
);

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE project_id_seq OWNED BY project.id;

ALTER TABLE ONLY project
    ALTER COLUMN id SET DEFAULT nextval('project_id_seq'::regclass);

ALTER TABLE ONLY project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);

ALTER TABLE ONLY project
    ADD CONSTRAINT project_company_fk FOREIGN KEY (company_id) REFERENCES company (id);
