package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Employee;

import java.util.List;

/**
 * A repository for storing and managing {@code Employee} objects.
 */
public interface EmployeeRepository {

    /**
     * Returns all employees of given company.
     * @param companyId an id of the company
     * @return a list containing all employees of given company
     */
    List<Employee> findAll(long companyId);

    /**
     * Creates new employee in repository.
     * @param employee the employee to be created
     */
    void create(Employee employee);

    /**
     * Updates existing employee in repository.
     * @param employee the employee to be updated
     */
    void update(Employee employee);

    /**
     * Deletes employee from the repository.
     * @param employee the employee to be deleted
     */
    void delete(Employee employee);

    /**
     * Deletes employee from the repository.
     * @param id an id of the employee to be deleted
     */
    void deleteById(Long id);

}
