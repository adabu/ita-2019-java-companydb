package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class CompanyBranchTransformerImplTest {

    private CompanyBranchTransformer companyBranchTransformer;
    @Mock
    private AddressTransformer addressTransformer;
    @Mock
    private CompanyTransformer companyTransformer;
    @Mock
    private EmployeeTransformer employeeTransformer;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);

        companyBranchTransformer = new CompanyBranchTransformerImpl(addressTransformer,
                companyTransformer, employeeTransformer);
    }

    @Test
    void transformationFromEntityToDtoReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final Address location = new Address(null, null, null, null, null);
        final Company company = new Company();
        final List<Employee> employees = List.of(new Employee());

        CompanyBranch companyBranch = new CompanyBranch();
        companyBranch.setId(id);
        companyBranch.setCreatedAt(createdAt);
        companyBranch.setUpdatedAt(updatedAt);
        companyBranch.setName(name);
        companyBranch.setLocation(location);
        companyBranch.setCompany(company);
        companyBranch.setEmployees(employees);

        AddressDto locationDto = new AddressDto(null, null, null, null, null);
        when(addressTransformer.toDto(location)).thenReturn(locationDto);

        CompanyDto companyDto = CompanyDto.builder().build();
        when(companyTransformer.toDtoWithId(company)).thenReturn(companyDto);

        List<EmployeeDto> employeeDtos = List.of(EmployeeDto.builder().build());
        when(employeeTransformer.toDtosWithId(employees)).thenReturn(employeeDtos);

        CompanyBranchDto companyBranchDto = companyBranchTransformer.toDto(companyBranch);

        assertThat(companyBranchDto.getId()).isEqualTo(id);
        assertThat(companyBranchDto.getCreatedAt()).isEqualTo(createdAt);
        assertThat(companyBranchDto.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(companyBranchDto.getName()).isEqualTo(name);
        assertThat(companyBranchDto.getLocation()).isSameAs(locationDto);
        assertThat(companyBranchDto.getCompany()).isSameAs(companyDto);
        assertThat(companyBranchDto.getEmployees()).isSameAs(employeeDtos);
    }

    @Test
    void transformationFromEntityToDtoWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        CompanyBranch companyBranch = new CompanyBranch();
        companyBranch.setId(null);
        companyBranch.setCreatedAt(null);
        companyBranch.setUpdatedAt(null);
        companyBranch.setName(null);
        companyBranch.setLocation(null);
        companyBranch.setCompany(null);
        companyBranch.setEmployees(null);

        CompanyBranchDto companyBranchDto = companyBranchTransformer.toDto(companyBranch);

        final Object[] companyBranchDtoPropertyValues = new Object[]{companyBranchDto.getId(),
                companyBranchDto.getCreatedAt(), companyBranchDto.getUpdatedAt(), companyBranchDto.getName(),
                companyBranchDto.getLocation(), companyBranchDto.getCompany()};
        assertThat(companyBranchDtoPropertyValues).containsOnlyNulls();
        assertThat(companyBranchDto.getEmployees()).isEmpty();
    }

    @Test
    void transformationFromDtoToEntityReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final AddressDto locationDto =
                new AddressDto(null, null, null, null, null);
        final Long companyId = 5L;
        final CompanyDto companyDto = CompanyDto.builder().build();
        final List<EmployeeDto> employeeDtos = List.of(EmployeeDto.builder().build());

        CompanyBranchDto companyBranchDto = CompanyBranchDto.builder()
                .setId(id)
                .setCreatedAt(createdAt)
                .setUpdatedAt(updatedAt)
                .setName(name)
                .setLocation(locationDto)
                .setCompany(companyDto)
                .setEmployees(employeeDtos)
                .build();

        Address location = new Address(null, null, null, null, null);
        when(addressTransformer.toEntity(locationDto)).thenReturn(location);

        Company company = new Company();
        when(companyTransformer.toEntityWithId(companyDto)).thenReturn(company);

        List<Employee> employees = new ArrayList<>(List.of(new Employee()));
        when(employeeTransformer.toEntitiesWithId(employeeDtos)).thenReturn(employees);

        CompanyBranch companyBranch = companyBranchTransformer.toEntity(companyBranchDto);

        assertThat(companyBranch.getId()).isEqualTo(id);
        assertThat(companyBranch.getCreatedAt()).isEqualTo(createdAt);
        assertThat(companyBranch.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(companyBranch.getName()).isEqualTo(name);
        assertThat(companyBranch.getLocation()).isSameAs(location);
        assertThat(companyBranch.getCompany()).isSameAs(company);
        assertThat(companyBranch.getEmployees()).isSameAs(employees);
    }

    @Test
    void transformationFromDtoToEntityWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        CompanyBranchDto companyBranchDto = CompanyBranchDto.builder()
                .setId(null)
                .setCreatedAt(null)
                .setUpdatedAt(null)
                .setName(null)
                .setLocation(null)
                .setCompany(null)
                .setEmployees(null)
                .build();

        CompanyBranch companyBranch = companyBranchTransformer.toEntity(companyBranchDto);

        final Object[] companyBranchPropertyValues = new Object[]{companyBranch.getId(), companyBranch.getCreatedAt(),
                companyBranch.getUpdatedAt(), companyBranch.getName(), companyBranch.getLocation(),
                companyBranch.getCompany()};
        assertThat(companyBranchPropertyValues).containsOnlyNulls();
        assertThat(companyBranch.getEmployees()).isEmpty();
    }

}
