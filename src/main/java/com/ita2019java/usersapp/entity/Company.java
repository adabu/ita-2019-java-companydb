package com.ita2019java.usersapp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "company")
public class Company extends AbstractEntity {

    @Column(name = "name", length = 100, nullable = false)
    private String name;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "street",
                    column = @Column(name = "address_street", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "address_city", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "address_house_number", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "address_zip_code", length = 6, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "address_country", length = 3, nullable = false)
            )
    })
    private Address headquarters;
    @Column(name = "company_id", length = 8, nullable = false)
    private String companyId;
    @Column(name = "vat_id", length = 10, nullable = false)
    private String vatId;
    @Column(name = "web_url")
    private String webUrl;
    @Column(name = "note")
    private String note;
    @Column(name = "active", nullable = false)
    private boolean active;
    @OneToMany(mappedBy = "company")
    private List<CompanyBranch> companyBranches;
    @OneToMany(mappedBy = "company")
    private List<Project> projects;

    public Company() {
    }

    public Company(Long id, String name, String companyId, String vatId, String webUrl, String note, boolean active) {
        setId(id);
        this.name = name;
        this.companyId = companyId;
        this.vatId = vatId;
        this.webUrl = webUrl;
        this.note = note;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Address getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(Address headquarters) {
        this.headquarters = headquarters;
    }

    public List<CompanyBranch> getCompanyBranches() {
        return companyBranches;
    }

    public void setCompanyBranches(List<CompanyBranch> companyBranches) {
        this.companyBranches = companyBranches;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return getName() + " (active: " + isActive() + ")";
    }

}
