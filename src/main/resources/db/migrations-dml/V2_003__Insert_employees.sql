INSERT INTO employee (created_at, updated_at, firstname, salary, surname, company_branch_id)
VALUES (now(), now(), 'Filip', 31420, 'Lípovský', 1),
       (now(), now(), 'Peter', 42000, 'Statočný', 1),
       (now(), now(), 'Julie', 36800, 'Novotná', 2),
       (now(), now(), 'Michal', 29507, 'Klemparský', 2),
       (now(), now(), 'Martin', 26560, 'Kotva', 3),
       (now(), now(), 'Kristína', 36800, 'Pastorková', 3);
