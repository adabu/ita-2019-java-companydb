INSERT INTO company_branch (created_at, updated_at,
                            address_city, address_country, address_house_number, address_street, address_zip_code,
                            name, company_id)
VALUES (now(), now(),
        'Brno', 'CZE', '490/27', 'Masarykova alej', '660 00',
        'Papírny Abeceda', 1),
       (now(), now(),
        'Praha', 'CZE', '110/8', 'Kulinářská čtvrť', '408 98',
        'Abeceda s.r.o.', 1),
       (now(), now(),
        'Prešov', 'SVK', '22/5', 'Námestie múdrých', '342 31',
        'Techno machines', 2);
