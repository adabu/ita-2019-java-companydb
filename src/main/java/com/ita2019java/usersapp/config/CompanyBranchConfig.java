package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.dto.mapping.*;
import com.ita2019java.usersapp.repository.CompanyBranchRepository;
import com.ita2019java.usersapp.service.CompanyBranchService;
import com.ita2019java.usersapp.service.CompanyBranchServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class CompanyBranchConfig {

    @Bean
    public CompanyBranchService companyBranchService(CompanyBranchRepository companyBranchRepository,
                                                     CompanyBranchTransformer companyBranchTransformer) {
        return new CompanyBranchServiceImpl(companyBranchRepository, companyBranchTransformer);
    }

    @Bean
    public CompanyBranchTransformer companyBranchTransformer(AddressTransformer addressTransformer,
                                                             CompanyTransformer companyTransformer,
                                                             @Lazy EmployeeTransformer employeeTransformer) {
        return new CompanyBranchTransformerImpl(addressTransformer, companyTransformer, employeeTransformer);
    }

}
