package com.ita2019java.usersapp.ares.util;

import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import cz.mfcr.ares.answer.AdresaARES;
import cz.mfcr.ares.answer.AresOdpovedi;
import cz.mfcr.ares.answer.Odpoved;
import cz.mfcr.ares.answer.Zaznam;

public class CompanyExtractor {

    /**
     * Creates a company based on data extracted from ares answer response.
     * @param aresOdpovedi an ares asnwer response
     * @return company with data set in accordance to given ares answer
     * @throws IllegalArgumentException if {@code odpoved} contains no company record
     */
    public Company extract(final AresOdpovedi aresOdpovedi) {
        Odpoved odpoved = aresOdpovedi.getOdpoved().get(0);
        return extract(odpoved);
    }

    /**
     * Creates a company based on data extracted from ares answer.
     * @param odpoved a concrete ares answer
     * @return company with data set in accordance to given ares answer
     * @throws IllegalArgumentException if {@code odpoved} contains no company record
     */
    public Company extract(final Odpoved odpoved) {
        if (odpoved.getPocetZaznamu() == 0) {
            throw new IllegalArgumentException("The given ares answer doesn't contain any company record.");
        }
        Zaznam zaznam = odpoved.getZaznam().get(0);
        return zaznamToCompany(zaznam);
    }

    private Company zaznamToCompany(Zaznam zaznam) {
        Company company = new Company();
        company.setName(zaznam.getObchodniFirma());

        final String ico = zaznam.getICO();
        company.setCompanyId(ico);

        final String czechVatIdPrefix = "CZ";
        final String vatId = czechVatIdPrefix + ico;
        company.setVatId(vatId);

        final AdresaARES adresaARES = zaznam.getIdentifikace().getAdresaARES();
        final String houseNumber = adresaARES.getCisloOrientacni() + "/" + adresaARES.getCisloDomovni();
        final String czechCountryCode = "CZE";
        Address address = new Address(adresaARES.getNazevUlice(), houseNumber,
                adresaARES.getNazevObce(), adresaARES.getPSC(), czechCountryCode);
        company.setHeadquarters(address);

        return company;
    }

}
