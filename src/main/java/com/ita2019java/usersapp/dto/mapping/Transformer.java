package com.ita2019java.usersapp.dto.mapping;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * @param <Dto> the DTO (data transfer object) type
 * @param <Entity> the entity type
 */
public interface Transformer<Dto, Entity> {

    /**
     * Returns DTO with properties set in accordance with {@code entity}.
     * If {@code entity} contains reference to another entity, the another entity is transformed to DTO with only set id.
     * @param entity the entity to transform
     * @return the transformed DTO
     */
    Dto toDto(Entity entity);

    /**
     * Returns entity with properties set in accordance with {@code dto}.
     * If {@code dto} contains reference to another DTO, the another DTO is transformed to Entity with only set id.
     * @param dto the DTO to transform
     * @return the transformed entity
     */
    Entity toEntity(Dto dto);

}
