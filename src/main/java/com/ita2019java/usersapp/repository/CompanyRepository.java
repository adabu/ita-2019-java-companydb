package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Company;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * A repository for storing and managing {@code Company} objects.
 */
public interface CompanyRepository {

    /**
     * Returns all companies stored in the repository.
     * @return a list containing all companies
     */
    List<Company> findAll();

    /**
     * Returns company stored in the repository.
     * @param id the id of company to be returned
     * @return company with the specified id or Optional#empty() if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    Optional<Company> findById(Long id);

    /**
     * Returns company stored in the repository.
     * @param companyId the company id of company to be returned
     * @return company with the specified companyId or Optional#empty() if none found
     */
    @Query("select c from Company c where c.companyId = ?1")
    Optional<Company> findByCompanyId(String companyId);

    /**
     * Based on the company's id, adds new company into the repository or updates existing company.
     * @param company the company to be added or updated
     * @return the saved company
     */
    Company save(Company company);

    /**
     * Deletes company from the repository.
     * @param id the id of company to be deleted
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    void deleteById(Long id);

}
