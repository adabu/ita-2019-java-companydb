package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.dto.mapping.CompanyBranchTransformer;
import com.ita2019java.usersapp.dto.mapping.EmployeeTransformer;
import com.ita2019java.usersapp.dto.mapping.EmployeeTransformerImpl;
import com.ita2019java.usersapp.dto.mapping.ProjectTransformer;
import com.ita2019java.usersapp.repository.EmployeeRepository;
import com.ita2019java.usersapp.repository.EmployeeRepositoryImpl;
import com.ita2019java.usersapp.service.EmployeeService;
import com.ita2019java.usersapp.service.EmployeeServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class EmployeeConfig {

    @Bean
    public EmployeeService employeeService(EmployeeRepository employeeRepository,
                                           EmployeeTransformer employeeTransformer) {
        return new EmployeeServiceImpl(employeeRepository, employeeTransformer);
    }

    @Bean
    public EmployeeRepository employeeRepository() {
        return new EmployeeRepositoryImpl();
    }

    @Bean
    public EmployeeTransformer employeeTransformer(CompanyBranchTransformer companyBranchTransformer,
                                                   @Lazy ProjectTransformer projectTransformer) {
        return new EmployeeTransformerImpl(companyBranchTransformer, projectTransformer);
    }

}
