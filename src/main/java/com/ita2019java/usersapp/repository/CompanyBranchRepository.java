package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.CompanyBranch;

import java.util.Optional;

/**
 * A repository for storing and managing {@code CompanyBranch} objects.
 */
public interface CompanyBranchRepository {

    /**
     * Returns company branch stored in the repository.
     * @param id the id of company branch to be returned
     * @return company branch with the specified id or Optional#empty() if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    Optional<CompanyBranch> findById(Long id);

    /**
     * Based on the id of the company branch, adds new company branch into the repository
     * or updates existing company branch.
     * @param entity the company branch to be added or updated
     * @return the saved company
     */
    CompanyBranch save(CompanyBranch entity);

    /**
     * Deletes a given entity.
     * @param entity a company branch to delete
     * @throws IllegalArgumentException in case the given entity is {@literal null}.
     */
    void delete(CompanyBranch entity);

    /**
     * Deletes company from the repository.
     * @param id the id of company branch to be deleted
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    void deleteById(Long id);

}
