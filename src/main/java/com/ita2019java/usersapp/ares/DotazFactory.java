package com.ita2019java.usersapp.ares;

import cz.mfcr.ares.request.Dotaz;

public class DotazFactory {

    public static Dotaz forCompanyOfId(String companyId) {
        return new DotazBuilder()
                .searchByCompanyId()
                .companyId(companyId)
                .build();
    }

}
