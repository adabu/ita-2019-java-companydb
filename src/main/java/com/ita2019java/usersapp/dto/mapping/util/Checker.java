package com.ita2019java.usersapp.dto.mapping.util;

import java.util.Collection;

public class Checker {

    public static <T> boolean containsNull(Collection<T> collection) {
        for (Object element : collection) {
            if (element == null) {
                return true;
            }
        }
        return false;
    }

}
