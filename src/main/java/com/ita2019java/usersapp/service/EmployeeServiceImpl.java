package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.mapping.EmployeeTransformer;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeTransformer employeeTransformer;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeTransformer employeeTransformer) {
        this.employeeRepository = employeeRepository;
        this.employeeTransformer = employeeTransformer;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findAll(long companyId) {
        LOGGER.info("Find all employees called.");
        List<EmployeeDto> employeeDtos = employeeRepository
                .findAll(companyId)
                .stream()
                .map(employeeTransformer::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned employees: {}.", employeeDtos.size());
        return employeeDtos;
    }

    @Override
    public void create(EmployeeDto employeeDto) {
        LOGGER.info("Create employee '{}' called.", employeeDto);
        Employee employee = employeeTransformer.toEntity(employeeDto);
        employeeRepository.create(employee);
        LOGGER.info("Created employee {}.", employee);
    }

    @Override
    public void update(EmployeeDto employeeDto) {
        LOGGER.info("Update employee to '{}' called.", employeeDto);
        Employee employee = employeeTransformer.toEntity(employeeDto);
        employeeRepository.update(employee);
        LOGGER.info("Employee updated to {}.", employee);
    }

    @Override
    public void delete(EmployeeDto employeeDto) {
        LOGGER.info("Delete employee '{}' called.", employeeDto);
        deleteById(employeeDto.getId());
        LOGGER.info("Employee {} was deleted.", employeeDto);
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("Delete employee by id called.");
        deleteById(id);
        LOGGER.info("Employee of given id was deleted.");
    }

    private void deleteById(Long id) {
        Objects.requireNonNull(id);
        employeeRepository.deleteById(id);
    }

}
