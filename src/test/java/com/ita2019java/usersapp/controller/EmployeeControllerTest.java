package com.ita2019java.usersapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private EmployeeService employeeService;

    @Test
    void getReturnsAllEmployees() throws Exception {
        final EmployeeDto employee = newEmployeeDto();
        final long id = getCompanyId(employee);
        when(employeeService.findAll(id)).thenReturn(List.of(employee));

        mockMvc.perform(
                get("/employee")
                        .param("companyId", String.valueOf(id)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(employee.getId()));

        verify(employeeService).findAll(eq(id));
    }

    @Test
    void createDelegatesCreationToService() throws Exception {
        final EmployeeDto employeeDto = newEmployeeDto();
        mockMvc.perform(
                post("/employee")
                        .content(objectMapper.writeValueAsString(employeeDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(employeeService).create(any(EmployeeDto.class));
    }

    @Test
    void updateDelegatesUpdateToService() throws Exception {
        final EmployeeDto employeeDto = newEmployeeDto();
        mockMvc.perform(
                put("/employee")
                        .content(objectMapper.writeValueAsString(employeeDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(employeeService).update(any(EmployeeDto.class));
    }

    @Test
    void deleteDelegatesDeleteToService() throws Exception {
        final long id = 1L;
        mockMvc.perform(
                delete("/employee")
                        .param("id", String.valueOf(id)))
                .andExpect(status().is2xxSuccessful());
        verify(employeeService).delete(eq(id));
    }

    private EmployeeDto newEmployeeDto() {
        final CompanyDto companyDto = CompanyDto.builder()
                .setId(3L)
                .build();
        final CompanyBranchDto companyBranchDto = CompanyBranchDto.builder()
                .setId(2L)
                .setCompany(companyDto)
                .build();

        return EmployeeDto.builder()
                .setId(1L)
                .setFirstname("Firstname")
                .setSurname("Surname")
                .setCompanyBranch(companyBranchDto)
                .build();
    }

    private Long getCompanyId(EmployeeDto employee) {
        return employee.getCompanyBranch().getCompany().getId();
    }

}
