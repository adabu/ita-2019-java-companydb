## About the application
Author: **Adam Bucher**

This application can be used as an database for companies which
can have their branches, projects and employees.
When searching for company by its registered id (in czech called "IČO")
that does not exists in the application company repository,
the application searches in for it in official czech company database
with service called ARES.  
A purpose of this application is to present my skills.
### Technologies used:
* Spring (core, Boot, MVC, WS),
* Maven,
* JPA (Hibernate, Spring Data JPA),
* PostgreSQL,
* Flyway,
* JUnit, AssertJ, Mockito.
### The functionality:
The application communicates via REST (see the
[API documentation](https://documenter.getpostman.com/view/9529282/SWLfcnwm)).
The REST endpoints are controlled by Spring's REST controllers.
Those communicate with according services which use repositories'
services for storing data about entities.

## Building the application
The application uses Maven. To build it, use `mvn clean package`.
Note that this command runs unit tests. If you want to skip them,
use `mvn clean package -D maven.test.skip=true`.

## Running the application
To run the application, use `mvn spring-boot:run`.  
You can set Spring profile to be run in `application.properties`
with the `spring.profiles.active` property.

The existing profiles are:
* `local` - for running in local environment,
* `heroku` - for running on cloud with Heroku.

There are two more profiles created for purposes of testing:
* `it-local` - for running integration tests in local environment,
* `it` - for running integration tests with remote database.

## A cloud deployment
This application uses Heroku as a cloud provider.
The deployment options can be edited in `.gitlab-ci.yml`.

### Continuous integration and deployment:
The application uses GitLab's pipeline. That automates application's building,
runs all tests and deploys the application to the cloud provider.

## Try it!
The application is deployed at: https://pure-cove-23528.herokuapp.com/.

To access some endpoints, you may need superuser credentials. Those are:  
* login: _sysadmin_,
* password: _passw0rd_.
