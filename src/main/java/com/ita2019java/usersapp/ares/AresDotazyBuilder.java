package com.ita2019java.usersapp.ares;

import cz.mfcr.ares.request.AresDotazTyp;
import cz.mfcr.ares.request.AresDotazy;
import cz.mfcr.ares.request.Dotaz;
import cz.mfcr.ares.request.VystupFormat;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AresDotazyBuilder {

    private List<Dotaz> dotazy = new ArrayList<>();
    private AresDotazTyp dotazTyp = AresDotazTyp.STANDARD;
    private VystupFormat vystupFormat = VystupFormat.TEXT;
    private String mail;
    private String id = "ares_dotaz";

    private static final String XSLT_LINK = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_request/v_1.0.0/ares_request.xsl";
    private static final String ANSWER_NAMESPACE_LINK = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_answer/v_1.0.1";

    public AresDotazyBuilder withDotaz(Dotaz dotaz) {
        dotazy.add(dotaz);
        return this;
    }

    public AresDotazyBuilder dotazy(List<Dotaz> dotazy) {
        this.dotazy = dotazy;
        return this;
    }

    public AresDotazyBuilder dotazTyp(AresDotazTyp dotazTyp) {
        this.dotazTyp = dotazTyp;
        return this;
    }

    public AresDotazyBuilder vystupFormat(VystupFormat vystupFormat) {
        this.vystupFormat = vystupFormat;
        return this;
    }

    public AresDotazyBuilder outputAsXml() {
        this.vystupFormat = VystupFormat.XML;
        return this;
    }

    public AresDotazyBuilder userMail(String mail) {
        this.mail = mail;
        return this;
    }

    public AresDotazyBuilder id(String id) {
        this.id = id;
        return this;
    }

    public AresDotazy build() {
        AresDotazy aresDotazy = new AresDotazy();

        aresDotazy.getDotaz().addAll(dotazy);

        LocalDateTime now = LocalDateTime.now();
        XMLGregorianCalendar calendar = DatatypeFactory.newDefaultInstance().newXMLGregorianCalendar(now.toString());
        aresDotazy.setDotazDatumCas(calendar);

        aresDotazy.setDotazPocet(dotazy.size());

        aresDotazy.setDotazTyp(dotazTyp);

        aresDotazy.setVystupFormat(vystupFormat);

        aresDotazy.setValidationXSLT(XSLT_LINK);

        aresDotazy.setUserMail(mail);

        aresDotazy.setAnswerNamespaceRequired(ANSWER_NAMESPACE_LINK);

        aresDotazy.setId(id);

        return aresDotazy;
    }

}
