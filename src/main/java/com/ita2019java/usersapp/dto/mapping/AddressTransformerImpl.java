package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.entity.Address;

public class AddressTransformerImpl implements AddressTransformer {

    @Override
    public AddressDto toDto(Address address) {
        return new AddressDto(
                address.getStreet(),
                address.getHouseNumber(),
                address.getCity(),
                address.getZipCode(),
                address.getCountry()
        );
    }

    @Override
    public Address toEntity(AddressDto addressDto) {
        return new Address(
                addressDto.getStreet(),
                addressDto.getHouseNumber(),
                addressDto.getCity(),
                addressDto.getZipCode(),
                addressDto.getCountry()
        );
    }

}
