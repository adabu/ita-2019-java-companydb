package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class EmployeeRepositoryImpl implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Employee> findAll(long companyId) {
        final String companyIdParameterName = "companyId";
        String query = "SELECT e FROM Employee e " +
                "JOIN FETCH e.companyBranch cb " +
                "JOIN FETCH cb.company c " +
                "WHERE e.companyBranch.company.id = :" + companyIdParameterName;
        return entityManager
                .createQuery(query, Employee.class)
                .setParameter(companyIdParameterName, companyId)
                .getResultList();
    }

    @Override
    public void create(Employee employee) {
        entityManager.persist(employee);
    }

    @Override
    public void update(Employee employee) {
        entityManager.merge(employee);
    }

    @Override
    public void delete(Employee employee) {
        entityManager.remove(employee);
    }

    @Override
    public void deleteById(Long id) {
        Employee employee = entityManager.find(Employee.class, id);
        entityManager.remove(employee);
    }

}
