package com.ita2019java.usersapp.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = EmployeeDto.EmployeeDtoBuilder.class)
public class EmployeeDto extends AbstractDto {

    private final String firstname;
    private final String surname;
    private final Long salary;
    private final CompanyBranchDto companyBranch;
    private final List<ProjectDto> projects;

    private EmployeeDto(Long id, LocalDateTime createdAt, LocalDateTime updatedAt,
                        String firstname, String surname, Long salary,
                        CompanyBranchDto companyBranch, List<ProjectDto> projects) {
        super(id, createdAt, updatedAt);
        this.firstname = firstname;
        this.surname = surname;
        this.salary = salary;
        this.companyBranch = companyBranch;
        this.projects = projects;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public Long getSalary() {
        return salary;
    }

    public CompanyBranchDto getCompanyBranch() {
        return companyBranch;
    }

    public List<ProjectDto> getProjects() {
        return projects;
    }

    public static EmployeeDtoBuilder builder() {
        return new EmployeeDtoBuilder();
    }

    @Override
    public String toString() {
        return getFirstname() + " " + getSurname() + " (" + getCompanyBranch().getName() + ")";
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class EmployeeDtoBuilder {

        private Long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String firstname;
        private String surname;
        private Long salary;
        private CompanyBranchDto companyBranch;
        private List<ProjectDto> projects;

        public EmployeeDtoBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public EmployeeDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public EmployeeDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public EmployeeDtoBuilder setFirstname(String firstname) {
            this.firstname = firstname;
            return this;
        }

        public EmployeeDtoBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public EmployeeDtoBuilder setSalary(Long salary) {
            this.salary = salary;
            return this;
        }

        public EmployeeDtoBuilder setCompanyBranch(CompanyBranchDto companyBranch) {
            this.companyBranch = companyBranch;
            return this;
        }

        public EmployeeDtoBuilder setProjects(List<ProjectDto> projects) {
            this.projects = projects;
            return this;
        }

        public EmployeeDto build() {
            return new EmployeeDto(
                    id, createdAt, updatedAt,
                    firstname, surname, salary,
                    companyBranch, projects
            );
        }

    }

}
