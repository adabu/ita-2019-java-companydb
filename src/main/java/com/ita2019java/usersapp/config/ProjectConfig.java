package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.dto.mapping.CompanyTransformer;
import com.ita2019java.usersapp.dto.mapping.EmployeeTransformer;
import com.ita2019java.usersapp.dto.mapping.ProjectTransformer;
import com.ita2019java.usersapp.dto.mapping.ProjectTransformerImpl;
import com.ita2019java.usersapp.repository.ProjectRepository;
import com.ita2019java.usersapp.repository.ProjectRepositoryImpl;
import com.ita2019java.usersapp.service.ProjectService;
import com.ita2019java.usersapp.service.ProjectServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectConfig {

    @Bean
    public ProjectService projectService(ProjectRepository projectRepository, ProjectTransformer projectTransformer) {
        return new ProjectServiceImpl(projectRepository, projectTransformer);
    }

    @Bean
    public ProjectRepository projectRepository() {
        return new ProjectRepositoryImpl();
    }

    @Bean
    public ProjectTransformer projectTransformer(CompanyTransformer companyTransformer,
                                                 EmployeeTransformer employeeTransformer) {
        return new ProjectTransformerImpl(companyTransformer, employeeTransformer);
    }

}
