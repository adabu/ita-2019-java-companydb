package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.EmployeeDto;

import java.util.List;

/**
 * A service for working with {@code EmployeeDto} objects.
 */
public interface EmployeeService {

    /**
     * Returns all employees of given company stored in employee repository.
     * @param companyId an id of the company
     * @return a list containing all employee DTOs of given company
     */
    List<EmployeeDto> findAll(long companyId);

    /**
     * Creates new employee in employee repository.
     * @param employeeDto the employee DTO to be created
     */
    void create(EmployeeDto employeeDto);

    /**
     * Updates existing employee in employee repository.
     * @param employeeDto the employee DTO to be updated
     */
    void update(EmployeeDto employeeDto);

    /**
     * Deletes employee from employee repository.
     * @param employeeDto the employee DTO to be deleted
     */
    void delete(EmployeeDto employeeDto);

    /**
     * Deletes employee from employee repository.
     * @param id an id of the employee
     */
    void delete(Long id);

}
