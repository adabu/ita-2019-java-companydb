package com.ita2019java.usersapp.controller;

import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<ProjectDto> getProjects() {
        LOGGER.info("Get all projects called.");
        List<ProjectDto> projectDtos = new ArrayList<>(projectService.findAll());
        LOGGER.info("Returning {} projects.", projectDtos.size());
        return projectDtos;
    }

    @GetMapping(params = {"name", "after"})
    public List<ProjectDto> getProjects(@RequestParam("name") String name,
                                        @RequestParam("after")
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime createdAfter) {
        LOGGER.info("Get all projects with name and time specification called.");
        List<ProjectDto> projectDtos = new ArrayList<>(projectService.findAll(name, createdAfter));
        LOGGER.info("Number of returned projects: {}.", projectDtos.size());
        return projectDtos;
    }

    @PostMapping
    public void createProject(@RequestBody ProjectDto project) {
        LOGGER.info("Create project {} called.", project);
        projectService.create(project);
        LOGGER.info("Project {} was created.", project);
    }

    @PutMapping
    public void updateProject(@RequestBody ProjectDto project) {
        LOGGER.info("Update project {} called.", project);
        projectService.update(project);
        LOGGER.info("Project {} was updated.", project);
    }

    @DeleteMapping
    public void deleteProject(@RequestParam("id") long projectId) {
        LOGGER.info("Delete project of id {} called.", projectId);
        projectService.delete(projectId);
        LOGGER.info("Project of id {} was deleted.", projectId);
    }

}
