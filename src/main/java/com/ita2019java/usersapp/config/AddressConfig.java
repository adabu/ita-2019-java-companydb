package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.dto.mapping.AddressTransformer;
import com.ita2019java.usersapp.dto.mapping.AddressTransformerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AddressConfig {

    @Bean
    public AddressTransformer addressTransformer() {
        return new AddressTransformerImpl();
    }

}
