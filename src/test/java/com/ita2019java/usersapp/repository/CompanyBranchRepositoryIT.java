package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.repository.util.SchemaUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assumptions.assumeThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CompanyBranchRepositoryIT {

    @Autowired
    private CompanyBranchRepository companyBranchRepository;
    @Autowired
    private SchemaUtils schemaUtils;

    @BeforeAll
    void beforeAll() {
        // re-create test schema
        schemaUtils.resetSchema();
    }

    @AfterAll
    void afterAll() {
        // drop test schema
        schemaUtils.cleanSchema();
    }

    @Test
    void findByIdReturnsCorrectCompany() {
        CompanyBranch companyBranch = createCompanyBranch();
        companyBranchRepository.save(companyBranch);

        // save should set an id of the company
        final Long id = companyBranch.getId();
        Optional<CompanyBranch> optionalCompanyBranch = companyBranchRepository.findById(id);
        assertThat(optionalCompanyBranch).isPresent();

        final String companyBranchName = companyBranch.getName();
        CompanyBranch foundCompanyBranch = optionalCompanyBranch.get();
        assertThat(foundCompanyBranch.getId()).isEqualTo(id);
        assertThat(foundCompanyBranch.getName()).isEqualTo(companyBranchName);
    }

    @Test
    void findByIdReturnsEmptyOptionalForNonexistentCompany() {
        final Long nonexistentId = -1L;
        Optional<CompanyBranch> optionalCompanyBranch = companyBranchRepository.findById(nonexistentId);
        assertThat(optionalCompanyBranch).isEmpty();
    }

    @Test
    void findByIdThrowsRuntimeExceptionWhenIdIsNull() {
        final Long id = null;
        assertThatThrownBy(() -> companyBranchRepository.findById(id)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void saveInsertsNewAndReturnsValidCompany() {
        CompanyBranch notStoredCompanyBranch = createCompanyBranch();
        final String name = notStoredCompanyBranch.getName();

        CompanyBranch storedCompanyBranch = companyBranchRepository.save(notStoredCompanyBranch);
        // save should set an id of the company
        assertThat(storedCompanyBranch.getId()).isNotNull();
        assertThat(storedCompanyBranch.getName()).isEqualTo(name);
    }

    @Test
    void saveInsertsNewAndStoresCompanySuccessfully() {
        CompanyBranch notStoredCompanyBranch = createCompanyBranch();
        final String name = notStoredCompanyBranch.getName();

        CompanyBranch storedCompanyBranch = companyBranchRepository.save(notStoredCompanyBranch);
        // save should set an id of the company
        final Long id = storedCompanyBranch.getId();

        Optional<CompanyBranch> optionalCompanyBranch = companyBranchRepository.findById(id);
        assertThat(optionalCompanyBranch).isPresent();
        CompanyBranch foundCompanyBranch = optionalCompanyBranch.get();
        assertThat(foundCompanyBranch.getName()).isEqualTo(name);
    }

    @Test
    void saveUpdatesAndReturnsValidCompany() {
        CompanyBranch companyBranch = companyBranchRepository.save(createCompanyBranch());
        // save should set an id of the company
        final Long id = companyBranch.getId();
        assumeThat(id).isNotNull();

        final String newName = "Updated name";
        assumeThat(companyBranch.getName()).isNotEqualTo(newName);
        companyBranch.setName(newName);

        assumeThat(companyBranchRepository.findById(id)).isPresent();
        CompanyBranch updatedCompanyBranch = companyBranchRepository.save(companyBranch);
        // update should not change the id
        assertThat(updatedCompanyBranch.getId()).isEqualTo(id);
        assertThat(updatedCompanyBranch.getName()).isEqualTo(newName);
    }

    @Test
    void saveUpdatesCompanySuccessfully() {
        CompanyBranch companyBranch = companyBranchRepository.save(createCompanyBranch());
        // save should set an id of the companyBranch
        final Long id = companyBranch.getId();
        assumeThat(id).isNotNull();

        final String newName = "Updated name";
        assumeThat(companyBranch.getName()).isNotEqualTo(newName);
        companyBranch.setName(newName);

        assumeThat(companyBranchRepository.findById(id)).isPresent();
        companyBranchRepository.save(companyBranch);
        Optional<CompanyBranch> updatedOptionalCompanyBranch = companyBranchRepository.findById(id);
        assertThat(updatedOptionalCompanyBranch).isPresent();
        CompanyBranch foundUpdatedCompanyBranch = updatedOptionalCompanyBranch.get();
        assertThat(foundUpdatedCompanyBranch.getName()).isEqualTo(newName);
    }

    @Test
    void deleteReallyDeletesCompany() {
        CompanyBranch companyBranch = companyBranchRepository.save(createCompanyBranch());
        // save should set an id of the company
        final Long id = companyBranch.getId();
        assumeThat(id).isNotNull();

        assumeThat(companyBranchRepository.findById(id)).isPresent();
        companyBranchRepository.delete(companyBranch);
        assertThat(companyBranchRepository.findById(id)).isEmpty();
    }

    @Test
    void deleteThrowsRuntimeExceptionWhenIdIsNull() {
        final CompanyBranch companyBranch = null;
        assertThatThrownBy(() -> companyBranchRepository.delete(companyBranch)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void deleteByIdReallyDeletesCompany() {
        CompanyBranch companyBranch = companyBranchRepository.save(createCompanyBranch());
        // save should set an id of the company
        final Long id = companyBranch.getId();
        assumeThat(id).isNotNull();

        assumeThat(companyBranchRepository.findById(id)).isPresent();
        companyBranchRepository.deleteById(id);
        assertThat(companyBranchRepository.findById(id)).isEmpty();
    }

    @Test
    void deleteByIdThrowsRuntimeExceptionWhenIdIsNull() {
        final Long id = null;
        assertThatThrownBy(() -> companyBranchRepository.deleteById(id)).isInstanceOf(RuntimeException.class);
    }

    private CompanyBranch createCompanyBranch() {
        final Company company = new Company();
        company.setId(1L);

        CompanyBranch companyBranch = new CompanyBranch();
        companyBranch.setName("Name");
        companyBranch.setLocation(new Address("Street", "3/5", "City", "Zip", "FFB"));
        companyBranch.setCompany(company);

        return companyBranch;
    }

}
