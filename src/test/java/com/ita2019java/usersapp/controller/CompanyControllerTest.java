package com.ita2019java.usersapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.service.CompanyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompanyService companyService;

    @Test
    void getReturnsAllCompanies() throws Exception {
        final CompanyDto company = newCompanyDto();
        when(companyService.findAll()).thenReturn(List.of(company));

        mockMvc.perform(get("/company"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(company.getId()));

        verify(companyService).findAll();
    }

    @Test
    void createDelegatesCreationToService() throws Exception {
        final CompanyDto companyDto = newCompanyDto();
        mockMvc.perform(
                post("/company")
                        .content(objectMapper.writeValueAsString(companyDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(companyService).create(any(CompanyDto.class));
    }

    @Test
    void updateDelegatesUpdateToService() throws Exception {
        final CompanyDto companyDto = newCompanyDto();
        mockMvc.perform(
                put("/company")
                        .content(objectMapper.writeValueAsString(companyDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(companyService).update(any(CompanyDto.class));
    }

    @Test
    void deleteDelegatesDeleteToService() throws Exception {
        final long id = 1L;
        mockMvc.perform(
                delete("/company")
                        .param("id", String.valueOf(id)))
                .andExpect(status().is2xxSuccessful());
        verify(companyService).delete(eq(id));
    }

    private CompanyDto newCompanyDto() {
        return CompanyDto.builder()
                .setId(1L)
                .setName("Name")
                .build();
    }

}
