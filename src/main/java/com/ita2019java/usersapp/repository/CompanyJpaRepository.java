package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * A JPA repository for storing and managing {@code Company} objects.
 */
public interface CompanyJpaRepository extends JpaRepository<Company, Long>, CompanyRepository {
}
