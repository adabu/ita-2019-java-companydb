package com.ita2019java.usersapp.ares;

import cz.mfcr.ares.request.AresDotazy;

public class AresDotazyFactory {

    public static AresDotazy forCompanyOfId(String companyId, String userMail) {
        return new AresDotazyBuilder()
                .withDotaz(DotazFactory.forCompanyOfId(companyId))
                .userMail(userMail)
                .outputAsXml()
                .build();
    }

}
