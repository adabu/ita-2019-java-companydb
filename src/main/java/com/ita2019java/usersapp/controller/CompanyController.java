package com.ita2019java.usersapp.controller;

import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService companyService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyDto> getCompanies() {
        LOGGER.info("Get companies called.");
        List<CompanyDto> companyDtos = new ArrayList<>(companyService.findAll());
        LOGGER.info("Returning {} companies.", companyDtos.size());
        return companyDtos;
    }

    @GetMapping("/{companyId}")
    public CompanyDto getCompany(@PathVariable String companyId) {
        LOGGER.info("Get company with company id {} called.", companyId);
        CompanyDto companyDto = companyService.find(companyId);
        LOGGER.info("Returning company {}.", companyDto);
        return companyDto;
    }

    @PostMapping
    public void createCompany(@RequestBody CompanyDto company) {
        LOGGER.info("Create company {} called.", company);
        companyService.create(company);
        LOGGER.info("Company {} was created.", company);
    }

    @PutMapping
    public void updateCompany(@RequestBody CompanyDto company) {
        LOGGER.info("Update company {} called.", company);
        companyService.update(company);
        LOGGER.info("Company {} was updated.", company);
    }

    @DeleteMapping
    public void deleteCompany(@RequestParam("id") long companyId) {
        LOGGER.info("Delete company of id {} called.", companyId);
        companyService.delete(companyId);
        LOGGER.info("Company of id {} was deleted.", companyId);
    }

}
