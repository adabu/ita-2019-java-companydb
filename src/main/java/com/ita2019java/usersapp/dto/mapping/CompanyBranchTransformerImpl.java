package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.mapping.util.Checker;
import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Employee;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * If the DTO/Entity object to be transformed contains reference to other DTO/Entity object,
 * the resulting Entity/DTO object contains reference to according Entity/DTO object, but only its id is set.
 */
public class CompanyBranchTransformerImpl implements CompanyBranchTransformer {

    private final AddressTransformer addressTransformer;
    private final CompanyTransformer companyTransformer;
    private final EmployeeTransformer employeeTransformer;

    public CompanyBranchTransformerImpl(AddressTransformer addressTransformer, CompanyTransformer companyTransformer,
                                        EmployeeTransformer employeeTransformer) {
        this.addressTransformer = addressTransformer;
        this.companyTransformer = companyTransformer;
        this.employeeTransformer = employeeTransformer;
    }

    @Override
    public CompanyBranchDto toDto(CompanyBranch companyBranch) {
        AddressDto addressDto = null;
        Address address = companyBranch.getLocation();
        if (address != null) {
            addressDto = addressTransformer.toDto(address);
        }

        CompanyDto companyDto = null;
        Company company = companyBranch.getCompany();
        if (company != null) {
            companyDto = companyTransformer.toDtoWithId(company);
        }

        List<EmployeeDto> employeeDtos = Collections.emptyList();
        List<Employee> employees = companyBranch.getEmployees();
        if (employees != null) {
            if (Checker.containsNull(employees)) {
                throw new IllegalArgumentException("Employees of company branch contain null element.");
            }
            employeeDtos = employeeTransformer.toDtosWithId(employees);
        }

        return CompanyBranchDto.builder()
                .setId(companyBranch.getId())
                .setCreatedAt(companyBranch.getCreatedAt())
                .setUpdatedAt(companyBranch.getUpdatedAt())
                .setName(companyBranch.getName())
                .setLocation(addressDto)
                .setCompany(companyDto)
                .setEmployees(employeeDtos)
                .build();
    }

    @Override
    public CompanyBranch toEntity(CompanyBranchDto companyBranchDto) {
        CompanyBranch companyBranch = new CompanyBranch();

        companyBranch.setId(companyBranchDto.getId());
        companyBranch.setCreatedAt(companyBranchDto.getCreatedAt());
        companyBranch.setUpdatedAt(companyBranchDto.getUpdatedAt());
        companyBranch.setName(companyBranchDto.getName());

        Address address = null;
        AddressDto addressDto = companyBranchDto.getLocation();
        if (addressDto != null) {
            address = addressTransformer.toEntity(addressDto);
        }
        companyBranch.setLocation(address);

        Company company = null;
        CompanyDto companyDto = companyBranchDto.getCompany();
        if (companyDto != null) {
            company = companyTransformer.toEntityWithId(companyDto);
        }
        companyBranch.setCompany(company);

        List<Employee> employees = Collections.emptyList();
        List<EmployeeDto> employeeDtos = companyBranchDto.getEmployees();
        if (employeeDtos != null) {
            if (Checker.containsNull(employeeDtos)) {
                throw new IllegalArgumentException("Employees of company branch DTO contain null element.");
            }
            employees = employeeTransformer.toEntitiesWithId(employeeDtos);
        }
        companyBranch.setEmployees(employees);

        return companyBranch;
    }

    @Override
    public CompanyBranchDto toDtoWithId(CompanyBranch companyBranch) {
        return CompanyBranchDto.builder()
                .setId(companyBranch.getId())
                .build();
    }

    @Override
    public CompanyBranch toEntityWithId(CompanyBranchDto companyBranchDto) {
        CompanyBranch companyBranch = new CompanyBranch();
        companyBranch.setId(companyBranchDto.getId());
        return companyBranch;
    }

    @Override
    public List<CompanyBranchDto> toDtosWithId(List<CompanyBranch> companyBranches) {
        return companyBranches.stream()
                .map(this::toDtoWithId)
                .collect(Collectors.toList());
    }

    @Override
    public List<CompanyBranch> toEntitiesWithId(List<CompanyBranchDto> companyBranchDtos) {
        return companyBranchDtos.stream()
                .map(this::toEntityWithId)
                .collect(Collectors.toList());
    }

}
