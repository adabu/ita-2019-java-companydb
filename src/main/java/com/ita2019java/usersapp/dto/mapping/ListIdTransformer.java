package com.ita2019java.usersapp.dto.mapping;

import java.util.List;

/**
 * Contains methods for transformation between data transfer object list and entity instances list.
 * @param <Dto> the DTO (data transfer object) type
 * @param <Entity> the entity type
 */
public interface ListIdTransformer<Dto, Entity> {

    /**
     * Returns DTO list with only id property set for each element in accordance with {@code entities}.
     * @param entities the list of entities to transform
     * @return the list of transformed DTOs
     */
    List<Dto> toDtosWithId(List<Entity> entities);

    /**
     * Returns entity list with only id property set for each element in accordance with {@code dtos}.
     * @param dtos the list of DTOs to transform
     * @return the list of transformed entities
     */
    List<Entity> toEntitiesWithId(List<Dto> dtos);

}
