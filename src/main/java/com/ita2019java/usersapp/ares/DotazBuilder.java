package com.ita2019java.usersapp.ares;

import cz.mfcr.ares.request.AresVyberTyp;
import cz.mfcr.ares.request.Dotaz;
import cz.mfcr.ares.request.KlicovePolozky;

public class DotazBuilder {

    private AresVyberTyp typVyhledani = AresVyberTyp.FREE;
    private final KlicovePolozky klicovePolozky = new KlicovePolozky();
    private int maxCount = 1;

    public DotazBuilder searchByCompanyId() {
        typVyhledani = AresVyberTyp.ICO;
        return this;
    }

    public DotazBuilder companyId(String companyId) {
        // argument validation
        final int companyIdMaxLength = 8;
        if (companyId.length() > companyIdMaxLength) {
            throw new IllegalArgumentException("The company id can't contain more than " +
                    companyIdMaxLength + " characters.");
        }
        if (!companyId.matches("[0-9]+")) {
            throw new IllegalArgumentException("The company id can only contain digits.");
        }

        klicovePolozky.setICO(companyId);
        return this;
    }

    public DotazBuilder maxCount(int maxCount) {
        this.maxCount = maxCount;
        return this;
    }

    public Dotaz build() {
        Dotaz dotaz = new Dotaz();

        dotaz.setTypVyhledani(typVyhledani);
        dotaz.setKlicovePolozky(klicovePolozky);
        dotaz.setMaxPocet(maxCount);

        return dotaz;
    }

}
