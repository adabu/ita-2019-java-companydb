CREATE TABLE project_employee
(
    project_id  bigint NOT NULL,
    employee_id bigint NOT NULL
);

ALTER TABLE ONLY project_employee
    ADD CONSTRAINT project_employee_employee_fk FOREIGN KEY (employee_id) REFERENCES employee (id);

ALTER TABLE ONLY project_employee
    ADD CONSTRAINT project_employee_project_fk FOREIGN KEY (project_id) REFERENCES project (id);
