package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.entity.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class EmployeeTransformerImplTest {

    private EmployeeTransformer employeeTransformer;
    @Mock
    private CompanyBranchTransformer companyBranchTransformer;
    @Mock
    private ProjectTransformer projectTransformer;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);

        employeeTransformer = new EmployeeTransformerImpl(companyBranchTransformer, projectTransformer);
    }

    @Test
    void transformationFromEntityToDtoReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String firstname = "Some firstname";
        final String surname = "Some surname";
        final Long salary = 1_000L;
        final CompanyBranch companyBranch = new CompanyBranch();
        final List<Project> projects = List.of(new Project());

        Employee employee = new Employee();
        employee.setId(id);
        employee.setCreatedAt(createdAt);
        employee.setUpdatedAt(updatedAt);
        employee.setFirstname(firstname);
        employee.setSurname(surname);
        employee.setSalary(salary);
        employee.setCompanyBranch(companyBranch);
        employee.setProjects(projects);

        CompanyBranchDto companyBranchDto = CompanyBranchDto.builder().build();
        when(companyBranchTransformer.toDtoWithId(companyBranch)).thenReturn(companyBranchDto);

        List<ProjectDto> projectDtos = List.of(ProjectDto.builder().build());
        when(projectTransformer.toDtosWithId(projects)).thenReturn(projectDtos);

        EmployeeDto employeeDto = employeeTransformer.toDto(employee);

        assertThat(employeeDto.getId()).isEqualTo(id);
        assertThat(employeeDto.getCreatedAt()).isEqualTo(createdAt);
        assertThat(employeeDto.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(employeeDto.getFirstname()).isEqualTo(firstname);
        assertThat(employeeDto.getSalary()).isEqualTo(salary);
        assertThat(employeeDto.getCompanyBranch()).isSameAs(companyBranchDto);
        assertThat(employeeDto.getProjects()).isSameAs(projectDtos);
    }

    @Test
    void transformationFromEntityToDtoWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        Employee employee = new Employee();
        employee.setId(null);
        employee.setCreatedAt(null);
        employee.setUpdatedAt(null);
        employee.setFirstname(null);
        employee.setSurname(null);
        employee.setSalary(null);
        employee.setCompanyBranch(null);
        employee.setProjects(null);

        EmployeeDto employeeDto = employeeTransformer.toDto(employee);

        final Object[] employeeDtoPropertyValues = new Object[]{employeeDto.getId(), employeeDto.getCreatedAt(),
                employeeDto.getUpdatedAt(), employeeDto.getFirstname(), employeeDto.getSurname(),
                employeeDto.getSalary(), employeeDto.getCompanyBranch()};
        assertThat(employeeDtoPropertyValues).containsOnlyNulls();
        assertThat(employeeDto.getProjects()).isEmpty();
    }

    @Test
    void transformationFromDtoToEntityReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String firstname = "Some firstname";
        final String surname = "Some surname";
        final Long salary = 1_000L;
        final CompanyBranchDto companyBranchDto = CompanyBranchDto.builder().build();
        final List<ProjectDto> projectDtos = List.of(ProjectDto.builder().build());

        EmployeeDto employeeDto = EmployeeDto.builder()
                .setId(id)
                .setCreatedAt(createdAt)
                .setUpdatedAt(updatedAt)
                .setFirstname(firstname)
                .setSurname(surname)
                .setSalary(salary)
                .setCompanyBranch(companyBranchDto)
                .setProjects(projectDtos)
                .build();

        CompanyBranch companyBranch = new CompanyBranch();
        when(companyBranchTransformer.toEntityWithId(companyBranchDto)).thenReturn(companyBranch);

        List<Project> projects = List.of(new Project());
        when(projectTransformer.toEntitiesWithId(projectDtos)).thenReturn(projects);

        Employee employee = employeeTransformer.toEntity(employeeDto);

        assertThat(employee.getId()).isEqualTo(id);
        assertThat(employee.getCreatedAt()).isEqualTo(createdAt);
        assertThat(employee.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(employee.getFirstname()).isEqualTo(firstname);
        assertThat(employee.getSalary()).isEqualTo(salary);
        assertThat(employee.getCompanyBranch()).isSameAs(companyBranch);
        assertThat(employee.getProjects()).isSameAs(projects);
    }

    @Test
    void transformationFromDtoToEntityWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .setId(null)
                .setCreatedAt(null)
                .setUpdatedAt(null)
                .setFirstname(null)
                .setSurname(null)
                .setSalary(null)
                .setCompanyBranch(null)
                .setProjects(null)
                .build();

        Employee employee = employeeTransformer.toEntity(employeeDto);

        final Object[] employeePropertyValues = new Object[]{employee.getId(), employee.getCreatedAt(),
                employee.getUpdatedAt(), employee.getFirstname(), employee.getSurname(),
                employee.getSalary(), employee.getCompanyBranch()};
        assertThat(employeePropertyValues).containsOnlyNulls();
        assertThat(employee.getProjects()).isEmpty();
    }

}
