package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.dto.mapping.ProjectTransformer;
import com.ita2019java.usersapp.entity.Project;
import com.ita2019java.usersapp.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Transactional
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectTransformer projectTransformer;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);

    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectTransformer projectTransformer) {
        this.projectRepository = projectRepository;
        this.projectTransformer = projectTransformer;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProjectDto> findAll() {
        LOGGER.info("Find all projects called.");
        List<ProjectDto> projectDtos = projectRepository
                .findAll()
                .stream()
                .map(projectTransformer::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned projects: {}.", projectDtos.size());
        return projectDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProjectDto> findAll(String name, LocalDateTime createdAfter) {
        LOGGER.info("Find all projects with name and time specification called.");
        List<ProjectDto> projectDtos = projectRepository
                .findAll(name, createdAfter)
                .stream()
                .map(projectTransformer::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned projects: {}.", projectDtos.size());
        return projectDtos;
    }

    @Override
    public void create(ProjectDto projectDto) {
        LOGGER.info("Create project '{}' called.", projectDto);
        Project project = projectTransformer.toEntity(projectDto);
        projectRepository.create(project);
        LOGGER.info("Created project {}.", project);
    }

    @Override
    public void update(ProjectDto projectDto) {
        LOGGER.info("Update project to '{}' called.", projectDto);
        Project project = projectTransformer.toEntity(projectDto);
        projectRepository.update(project);
        LOGGER.info("Project updated to {}.", project);
    }

    @Override
    public void delete(ProjectDto projectDto) {
        LOGGER.info("Delete project '{}' called.", projectDto);
        deleteById(projectDto.getId());
        LOGGER.info("Project {} was deleted.", projectDto);
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("Delete project by id called.");
        deleteById(id);
        LOGGER.info("Project of given id was deleted.");
    }

    private void deleteById(Long id) {
        Objects.requireNonNull(id);
        projectRepository.deleteById(id);
    }

}
