package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.entity.Employee;

/**
 * Contains methods for transformation between instances of {@code EmployeeDto} and {@code Employee}
 * and lists of their instances.
 */
public interface EmployeeTransformer extends HeavyTransformer<EmployeeDto, Employee> {
}
