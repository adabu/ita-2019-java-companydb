package com.ita2019java.usersapp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee extends AbstractEntity {

    @Column(name = "firstname", length = 30, nullable = false)
    private String firstname;
    @Column(name = "surname", length = 40, nullable = false)
    private String surname;
    @Column(name = "salary")
    private Long salary;
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_branch_id", foreignKey = @ForeignKey(name = "employee_company_branch_fk"))
    private CompanyBranch companyBranch;
    @ManyToMany(mappedBy = "employees")
    private List<Project> projects;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public CompanyBranch getCompanyBranch() {
        return companyBranch;
    }

    public void setCompanyBranch(CompanyBranch companyBranch) {
        this.companyBranch = companyBranch;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return getFirstname() + " " + getSurname() + " (" + getCompanyBranch().getName() + ")";
    }

}
