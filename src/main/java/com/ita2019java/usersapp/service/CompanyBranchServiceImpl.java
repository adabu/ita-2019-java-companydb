package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.mapping.CompanyBranchTransformer;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.repository.CompanyBranchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Transactional
public class CompanyBranchServiceImpl implements CompanyBranchService {

    private CompanyBranchRepository companyBranchRepository;
    private final CompanyBranchTransformer companyBranchTransformer;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyBranchServiceImpl.class);

    public CompanyBranchServiceImpl(CompanyBranchRepository companyBranchRepository,
                                    CompanyBranchTransformer companyBranchTransformer) {
        this.companyBranchRepository = companyBranchRepository;
        this.companyBranchTransformer = companyBranchTransformer;
    }

    @Override
    @Transactional(readOnly = true)
    public CompanyBranchDto findById(Long id) {
        LOGGER.info("Find company branch by id called.");
        Optional<CompanyBranch> optionalCompanyBranch = companyBranchRepository.findById(id);
        CompanyBranch companyBranch = optionalCompanyBranch.orElseThrow(() -> {
            throw new IllegalArgumentException("No company branch of given id '" + id + "' exists.");
        });
        LOGGER.info("Found company branch {}", companyBranch);

        CompanyBranchDto companyBranchDto = companyBranchTransformer.toDto(companyBranch);
        LOGGER.info("Returning transformed company branch DTO {}", companyBranchDto);
        return companyBranchDto;
    }

    @Override
    public void create(CompanyBranchDto companyBranchDto) {
        LOGGER.info("Create company branch '{}' called.", companyBranchDto);
        CompanyBranch companyBranch = companyBranchTransformer.toEntity(companyBranchDto);
        companyBranchRepository.save(companyBranch);
        LOGGER.info("Created company branch {}.", companyBranch);
    }

    @Override
    public void update(CompanyBranchDto companyBranchDto) {
        LOGGER.info("Update company branch to '{}' called.", companyBranchDto);
        CompanyBranch companyBranch = companyBranchTransformer.toEntity(companyBranchDto);
        companyBranchRepository.save(companyBranch);
        LOGGER.info("Company branch updated to {}.", companyBranch);
    }

    @Override
    public void delete(CompanyBranchDto companyBranchDto) {
        LOGGER.info("Delete company branch '{}' called.", companyBranchDto);
        deleteById(companyBranchDto.getId());
        LOGGER.info("Company branch {} was deleted.", companyBranchDto);
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("Delete company branch by id called.");
        deleteById(id);
        LOGGER.info("Company branch of given id was deleted.");
    }

    private void deleteById(Long id) {
        Objects.requireNonNull(id);
        companyBranchRepository.deleteById(id);
    }

}
