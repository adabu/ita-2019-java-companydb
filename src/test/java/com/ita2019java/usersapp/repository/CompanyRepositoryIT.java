package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.repository.util.SchemaUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assumptions.assumeThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CompanyRepositoryIT {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private SchemaUtils schemaUtils;

    @BeforeAll
    void beforeAll() {
        // re-create test schema
        schemaUtils.resetSchema();
    }

    @AfterAll
    void afterAll() {
        // drop test schema
        schemaUtils.cleanSchema();
    }

    @Test
    void findAllReturnsAll() {
        // companies count may be affected by other tests...
        schemaUtils.resetSchema();

        final int insertedCompaniesCount = 2;
        List<Company> returnedCompanies = companyRepository.findAll();
        assertThat(returnedCompanies).hasSize(insertedCompaniesCount);
    }

    @Test
    void findByIdReturnsCorrectCompany() {
        final Long id = 2L;
        Optional<Company> returnedOptionalCompany = companyRepository.findById(id);
        assertThat(returnedOptionalCompany).isPresent();

        Company returnedCompany = returnedOptionalCompany.get();
        final String companyId = "2NDCOMP0";
        assertThat(returnedCompany.getCompanyId()).isEqualTo(companyId);
        final String name = "Techno";
        assertThat(returnedCompany.getName()).isEqualTo(name);
    }

    @Test
    void findByIdReturnsEmptyOptionalForNonexistentId() {
        final Long id = -1L;
        Optional<Company> returnedOptionalCompany = companyRepository.findById(id);
        assertThat(returnedOptionalCompany).isEmpty();
    }

    @Test
    void findByIdThrowsRuntimeExceptionWhenIdIsNull() {
        final Long id = null;
        assertThatThrownBy(() -> companyRepository.findById(id)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void findByCompanyIdReturnsCorrectCompany() {
        final String companyId = "2NDCOMP0";
        Optional<Company> returnedOptionalCompany = companyRepository.findByCompanyId(companyId);
        assertThat(returnedOptionalCompany).isPresent();

        Company returnedCompany = returnedOptionalCompany.get();
        final Long id = 2L;
        assertThat(returnedCompany.getId()).isEqualTo(id);
        final String name = "Techno";
        assertThat(returnedCompany.getName()).isEqualTo(name);
    }

    @Test
    void findByCompanyIdReturnsEmptyOptionalForNonexistentId() {
        final String companyId = "00000000";
        Optional<Company> returnedOptionalCompany = companyRepository.findByCompanyId(companyId);
        assertThat(returnedOptionalCompany).isEmpty();
    }

    @Test
    void saveInsertsNewAndReturnsValidCompany() {
        Company notStoredCompany = createCompany();
        final String name = notStoredCompany.getName();

        Company storedCompany = companyRepository.save(notStoredCompany);
        // save should set an id of the company
        assertThat(storedCompany.getId()).isNotNull();
        assertThat(storedCompany.getName()).isEqualTo(name);
    }

    @Test
    void saveInsertsNewAndStoresCompanySuccessfully() {
        Company notStoredCompany = createCompany();
        final String name = notStoredCompany.getName();

        Company storedCompany = companyRepository.save(notStoredCompany);
        // save should set an id of the company
        final Long id = storedCompany.getId();

        Optional<Company> foundOptionalCompany = companyRepository.findById(id);
        assertThat(foundOptionalCompany).isPresent();
        Company foundCompany = foundOptionalCompany.get();
        assertThat(foundCompany.getName()).isEqualTo(name);
    }

    @Test
    void saveUpdatesAndReturnsValidCompany() {
        Company company = companyRepository.save(createCompany());
        // save should set an id of the company
        final Long id = company.getId();
        assumeThat(id).isNotNull();

        final String newName = "Updated name";
        assumeThat(company.getName()).isNotEqualTo(newName);
        company.setName(newName);

        assumeThat(companyRepository.findById(id)).isPresent();
        Company updatedCompany = companyRepository.save(company);
        // update should not change the id
        assertThat(updatedCompany.getId()).isEqualTo(id);
        assertThat(updatedCompany.getName()).isEqualTo(newName);
    }

    @Test
    void saveUpdatesCompanySuccessfully() {
        Company company = companyRepository.save(createCompany());
        // save should set an id of the company
        final Long id = company.getId();
        assumeThat(id).isNotNull();

        final String newName = "Updated name";
        assumeThat(company.getName()).isNotEqualTo(newName);
        company.setName(newName);

        assumeThat(companyRepository.findById(id)).isPresent();
        companyRepository.save(company);
        Optional<Company> foundUpdatedOptionalCompany = companyRepository.findById(id);
        assertThat(foundUpdatedOptionalCompany).isPresent();
        Company foundUpdatedCompany = foundUpdatedOptionalCompany.get();
        assertThat(foundUpdatedCompany.getName()).isEqualTo(newName);
    }

    @Test
    void deleteByIdReallyDeletesCompany() {
        Company company = companyRepository.save(createCompany());
        // save should set an id of the company
        final Long id = company.getId();
        assumeThat(id).isNotNull();

        assumeThat(companyRepository.findById(id)).isPresent();
        companyRepository.deleteById(id);
        assertThat(companyRepository.findById(id)).isEmpty();
    }

    @Test
    void deleteByIdThrowsRuntimeExceptionWhenIdIsNull() {
        final Long id = null;
        assertThatThrownBy(() -> companyRepository.deleteById(id)).isInstanceOf(RuntimeException.class);
    }

    private Company createCompany() {
        Company notStoredCompany = new Company();
        final String name = "New company with new name";
        notStoredCompany.setName(name);
        final String companyId = "12345678";
        notStoredCompany.setCompanyId(companyId);
        final String vatId = "1234567890";
        notStoredCompany.setVatId(vatId);
        final Address address = new Address("Street", "1/1", "City", "Zip", "CNT");
        notStoredCompany.setHeadquarters(address);

        return notStoredCompany;
    }

}
