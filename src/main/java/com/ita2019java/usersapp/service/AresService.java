package com.ita2019java.usersapp.service;

import cz.mfcr.ares.answer.AresOdpovedi;
import cz.mfcr.ares.request.AresDotazy;

/**
 * Provides communication with ARES.
 * @see <a href="http://wwwinfo.mfcr.cz/">ARES</a>
 */
public interface AresService {

    /**
     * Returns ares answers for given ares queries.
     * @param aresDotazy the ares queries
     * @return the ares answers
     */
    AresOdpovedi getAresOdpovedi(AresDotazy aresDotazy);

    /**
     * Returns ares answers for ares query created from given company id.
     * @param companyId the company id
     * @return the ares answers
     */
    AresOdpovedi getAresOdpovediForCompanyId(String companyId);

}
