package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.entity.CompanyBranch;

/**
 * Contains methods for transformation between instances of {@code CompanyBranchDto} and {@code CompanyBranch}
 * and lists of their instances.
 */
public interface CompanyBranchTransformer extends HeavyTransformer<CompanyBranchDto, CompanyBranch> {
}
