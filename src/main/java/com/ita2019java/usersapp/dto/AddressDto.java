package com.ita2019java.usersapp.dto;

public class AddressDto {

    private final String street;
    private final String city;
    private final String zipCode;
    private final String country;
    private final String houseNumber;

    public AddressDto(String street, String houseNumber, String city, String zipCode, String country) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountry() {
        return country;
    }

}
