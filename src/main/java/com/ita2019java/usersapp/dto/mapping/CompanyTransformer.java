package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.entity.Company;

/**
 * Contains methods for transformation between instances of {@code CompanyDto} and {@code Company}
 * and lists of their instances.
 */
public interface CompanyTransformer extends HeavyTransformer<CompanyDto, Company> {
}
