package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.entity.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class ProjectTransformerImplTest {

    private ProjectTransformer projectTransformer;
    @Mock
    private CompanyTransformer companyTransformer;
    @Mock
    private EmployeeTransformer employeeTransformer;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);

        projectTransformer = new ProjectTransformerImpl(companyTransformer, employeeTransformer);
    }

    @Test
    void transformationFromEntityToDtoReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final String startDate = "Some start date";
        final String endDate = "Some end date";
        final Long price = 1_000L;
        final Company company = new Company();
        final List<Employee> employees = List.of(new Employee());

        Project project = new Project();
        project.setId(id);
        project.setCreatedAt(createdAt);
        project.setUpdatedAt(updatedAt);
        project.setName(name);
        project.setStartDate(startDate);
        project.setEndDate(endDate);
        project.setPrice(price);
        project.setCompany(company);
        project.setEmployees(employees);

        CompanyDto companyDto = CompanyDto.builder().build();
        when(companyTransformer.toDtoWithId(company)).thenReturn(companyDto);

        List<EmployeeDto> employeeDtos = List.of(EmployeeDto.builder().build());
        when(employeeTransformer.toDtosWithId(employees)).thenReturn(employeeDtos);

        ProjectDto projectDto = projectTransformer.toDto(project);

        assertThat(projectDto.getId()).isEqualTo(id);
        assertThat(projectDto.getCreatedAt()).isEqualTo(createdAt);
        assertThat(projectDto.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(projectDto.getName()).isEqualTo(name);
        assertThat(projectDto.getStartDate()).isEqualTo(startDate);
        assertThat(projectDto.getEndDate()).isEqualTo(endDate);
        assertThat(projectDto.getPrice()).isEqualTo(price);
        assertThat(projectDto.getCompany()).isSameAs(companyDto);
        assertThat(projectDto.getEmployees()).isSameAs(employeeDtos);
    }

    @Test
    void transformationFromEntityToDtoWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        Project project = new Project();
        project.setId(null);
        project.setCreatedAt(null);
        project.setUpdatedAt(null);
        project.setName(null);
        project.setStartDate(null);
        project.setEndDate(null);
        project.setPrice(null);
        project.setCompany(null);
        project.setEmployees(null);

        ProjectDto projectDto = projectTransformer.toDto(project);

        final Object[] projectDtoPropertyValues = new Object[]{projectDto.getId(), projectDto.getCreatedAt(),
                projectDto.getUpdatedAt(), projectDto.getName(), projectDto.getStartDate(), projectDto.getEndDate(),
                projectDto.getPrice(), projectDto.getCompany()};
        assertThat(projectDtoPropertyValues).containsOnlyNulls();
        assertThat(projectDto.getEmployees()).isEmpty();
    }

    @Test
    void transformationFromDtoToEntityReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final String startDate = "Some start date";
        final String endDate = "Some end date";
        final Long price = 1_000L;
        final CompanyDto companyDto = CompanyDto.builder().build();
        final List<EmployeeDto> employeeDtos = List.of(EmployeeDto.builder().build());

        ProjectDto projectDto = ProjectDto.builder()
                .setId(id)
                .setCreatedAt(createdAt)
                .setUpdatedAt(updatedAt)
                .setName(name)
                .setStartDate(startDate)
                .setEndDate(endDate)
                .setPrice(price)
                .setCompany(companyDto)
                .setEmployees(employeeDtos)
                .build();

        Company company = new Company();
        when(companyTransformer.toEntityWithId(companyDto)).thenReturn(company);

        List<Employee> employees = List.of(new Employee());
        when(employeeTransformer.toEntitiesWithId(employeeDtos)).thenReturn(employees);

        Project project = projectTransformer.toEntity(projectDto);

        assertThat(project.getId()).isEqualTo(id);
        assertThat(project.getCreatedAt()).isEqualTo(createdAt);
        assertThat(project.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(project.getName()).isEqualTo(name);
        assertThat(project.getStartDate()).isEqualTo(startDate);
        assertThat(project.getEndDate()).isEqualTo(endDate);
        assertThat(project.getPrice()).isEqualTo(price);
        assertThat(project.getCompany()).isSameAs(company);
        assertThat(project.getEmployees()).isSameAs(employees);
    }

    @Test
    void transformationFromDtoToEntityWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        ProjectDto projectDto = ProjectDto.builder()
                .setId(null)
                .setCreatedAt(null)
                .setUpdatedAt(null)
                .setName(null)
                .setStartDate(null)
                .setEndDate(null)
                .setPrice(null)
                .setCompany(null)
                .setEmployees(null)
                .build();

        Project project = projectTransformer.toEntity(projectDto);

        final Object[] projectPropertyValues = new Object[]{project.getId(), project.getCreatedAt(),
                project.getUpdatedAt(), project.getName(), project.getStartDate(), project.getEndDate(),
                project.getPrice(), project.getCompany()};
        assertThat(projectPropertyValues).containsOnlyNulls();
        assertThat(project.getEmployees()).isEmpty();
    }

}
