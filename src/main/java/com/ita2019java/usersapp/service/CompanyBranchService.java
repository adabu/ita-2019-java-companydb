package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.CompanyBranchDto;

/**
 * A service for working with {@code CompanyBranchDto} objects.
 */
public interface CompanyBranchService {

    /**
     * Returns the company branch stored in company branch repository.
     * @param id an id of the company branch
     * @return a company branch DTO with the id
     * @throws IllegalArgumentException if no company branch of the {@code id} exists
     */
    CompanyBranchDto findById(Long id);

    /**
     * Creates new company branch in company branch repository.
     * @param companyBranchDto the company branch DTO to be created
     */
    void create(CompanyBranchDto companyBranchDto);

    /**
     * Updates existing company branch in company branch repository.
     * @param companyBranchDto the company branch DTO to be updated
     */
    void update(CompanyBranchDto companyBranchDto);

    /**
     * Deletes company branch from company branch repository.
     * @param companyBranchDto the company branch DTO to be deleted
     */
    void delete(CompanyBranchDto companyBranchDto);

    /**
     * Deletes company branch from company branch repository.
     * @param id an id of the company branch
     */
    void delete(Long id);

}
