package com.ita2019java.usersapp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "company_branch")
public class CompanyBranch extends AbstractEntity {

    @Column(name = "name", length = 100, nullable = false)
    private String name;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "street",
                    column = @Column(name = "address_street", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "address_city", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "address_house_number", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "address_zip_code", length = 6, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "address_country", length = 3, nullable = false)
            )
    })
    private Address location;
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "company_branch_company_fk"))
    private Company company;
    @OneToMany(mappedBy = "companyBranch")
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return getName() + "(" + company.getName() + ")";
    }

}
