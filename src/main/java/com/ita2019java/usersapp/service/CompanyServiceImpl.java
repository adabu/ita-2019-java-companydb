package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.ares.util.CompanyExtractor;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.mapping.CompanyTransformer;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.exception.RecordNotFoundException;
import com.ita2019java.usersapp.repository.CompanyRepository;
import cz.mfcr.ares.answer.AresOdpovedi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
public class CompanyServiceImpl implements CompanyService {

    private CompanyRepository companyRepository;
    private final CompanyTransformer companyTransformer;
    private AresService aresService;
    private final CompanyExtractor companyExtractor;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    public CompanyServiceImpl(CompanyRepository companyRepository, CompanyTransformer companyTransformer,
                              AresService aresService, CompanyExtractor companyExtractor) {
        this.companyRepository = companyRepository;
        this.companyTransformer = companyTransformer;
        this.aresService = aresService;
        this.companyExtractor = companyExtractor;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CompanyDto> findAll() {
        LOGGER.info("Find all companies called.");
        List<CompanyDto> companyDtos = companyRepository
                .findAll()
                .stream()
                .map(companyTransformer::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned companies: {}.", companyDtos.size());
        return companyDtos;
    }

    @Override
    public CompanyDto find(String companyId) {
        LOGGER.info("Find company with company id '{}' called.", companyId);
        Company company;

        Optional<Company> optionalCompany = companyRepository.findByCompanyId(companyId);
        boolean companyExistsInRepository = false;
        if (optionalCompany.isPresent()) {
            LOGGER.info("Found company in the repository.");
            companyExistsInRepository = true;
            company = optionalCompany.get();
        } else {
            LOGGER.info("Searching for company of id {} in ARES.", companyId);
            AresOdpovedi aresOdpovediForCompanyId = aresService.getAresOdpovediForCompanyId(companyId);
            try {
                company = companyExtractor.extract(aresOdpovediForCompanyId);
                LOGGER.info("Found company of id {} via ARES.", companyId);
            } catch (IllegalArgumentException ex) {
                throw new RecordNotFoundException("No company of id " + companyId + " was found.", ex);
            }
        }

        CompanyDto companyDto = companyTransformer.toDto(company);

        if (!companyExistsInRepository) {
            create(companyDto);
        }

        return companyDto;
    }

    @Override
    public void create(CompanyDto companyDto) {
        LOGGER.info("Create company '{}' called.", companyDto);
        Company company = companyTransformer.toEntity(companyDto);
        companyRepository.save(company);
        LOGGER.info("Created company {}.", company);
    }

    @Override
    public void update(CompanyDto companyDto) {
        LOGGER.info("Update companyDto to '{}' called.", companyDto);
        Company company = companyTransformer.toEntity(companyDto);
        companyRepository.save(company);
        LOGGER.info("Company updated to {}.", companyDto);
    }

    @Override
    public void delete(long id) {
        LOGGER.info("Delete company with ID '{}' called.", id);
        companyRepository.deleteById(id);
        LOGGER.info("Company with ID {} was deleted.", id);
    }

}
