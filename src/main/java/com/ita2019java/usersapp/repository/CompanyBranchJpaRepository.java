package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.CompanyBranch;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * A JPA repository for storing and managing {@code CompanyBranch} objects.
 */
public interface CompanyBranchJpaRepository extends JpaRepository<CompanyBranch, Long>, CompanyBranchRepository {
}
