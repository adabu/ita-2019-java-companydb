CREATE TABLE employee
(
    id                bigint                NOT NULL,
    created_at        timestamp without time zone,
    updated_at        timestamp without time zone,
    firstname         character varying(30) NOT NULL,
    salary            bigint,
    surname           character varying(40) NOT NULL,
    company_branch_id bigint                NOT NULL
);

CREATE SEQUENCE employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE employee_id_seq OWNED BY employee.id;

ALTER TABLE ONLY employee
    ALTER COLUMN id SET DEFAULT nextval('employee_id_seq'::regclass);

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (id);

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_company_branch_fk FOREIGN KEY (company_branch_id) REFERENCES company_branch (id);
