package com.ita2019java.usersapp.controller;

import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.service.CompanyBranchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/company-branch")
public class CompanyBranchController {

    private final CompanyBranchService companyBranchService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyBranchController.class);

    public CompanyBranchController(CompanyBranchService companyBranchService) {
        this.companyBranchService = companyBranchService;
    }

    @GetMapping
    public CompanyBranchDto getCompanyBranch(@RequestParam long id) {
        LOGGER.info("Get company branches called.");
        CompanyBranchDto companyBranchDto = companyBranchService.findById(id);
        LOGGER.info("Returning company branch {}.", companyBranchDto);
        return companyBranchDto;
    }

    @PostMapping
    public void createCompanyBranch(@RequestBody CompanyBranchDto companyBranch) {
        LOGGER.info("Create company branch {} called.", companyBranch);
        companyBranchService.create(companyBranch);
        LOGGER.info("Company branch {} was created.", companyBranch);
    }

    @PutMapping
    public void updateCompany(@RequestBody CompanyBranchDto companyBranch) {
        LOGGER.info("Update company {} called.", companyBranch);
        companyBranchService.update(companyBranch);
        LOGGER.info("Company {} was updated.", companyBranch);
    }

    @DeleteMapping
    public void deleteCompany(@RequestParam("id") long companyId) {
        LOGGER.info("Delete company of id {} called.", companyId);
        companyBranchService.delete(companyId);
        LOGGER.info("Company of id {} was deleted.", companyId);
    }

}
