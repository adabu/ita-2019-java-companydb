package com.ita2019java.usersapp.service;

import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.exception.RecordNotFoundException;

import java.util.List;

/**
 * A service for working with {@code CompanyDto} objects.
 */
public interface CompanyService {

    /**
     * Returns all companies stored in companies' repository sorted by name in ascending order.
     * @return a list containing all company DTOs
     */
    List<CompanyDto> findAll();

    /**
     * Returns company from companies' repository.
     * If company is not found, searches for the company in czech database of companies.
     * If company of {@code companyId} is found there, it will be stored in repository and returned.
     * @param companyId a registered company id
     * @return a company with the company id
     * @throws RecordNotFoundException if no company of the {@code companyId} is found
     */
    CompanyDto find(String companyId);

    /**
     * Adds new company into companies' repository.
     * @param companyDto the company DTO to be added
     */
    void create(CompanyDto companyDto);

    /**
     * Updates data of company in companies' repository.
     * @param companyDto the company DTO which data will be updated
     * @throws RecordNotFoundException if no company of its id is found in companies' repository
     */
    void update(CompanyDto companyDto) throws RecordNotFoundException;

    /**
     * Deletes company from companies' repository.
     * @param id the id of company DTO to be deleted
     * @throws RecordNotFoundException if no company of the id is found in companies' repository
     */
    void delete(long id) throws RecordNotFoundException;

}
