package com.ita2019java.usersapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.service.CompanyBranchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CompanyBranchController.class)
class CompanyBranchControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompanyBranchService companyBranchService;

    @Test
    void getWithParamIdReturnsCompanyBranch() throws Exception {
        final CompanyBranchDto companyBranch = newCompanyBranchDto();
        final long companyBranchId = companyBranch.getId();
        when(companyBranchService.findById(companyBranchId)).thenReturn(companyBranch);

        mockMvc.perform(
                get("/company-branch")
                        .param("id", String.valueOf(companyBranchId)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.id").value(companyBranchId));

        verify(companyBranchService).findById(companyBranch.getId());
    }

    @Test
    void createDelegatesCreationToService() throws Exception {
        final CompanyBranchDto companyBranchDto = newCompanyBranchDto();
        mockMvc.perform(
                post("/company-branch")
                        .content(objectMapper.writeValueAsString(companyBranchDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(companyBranchService).create(any(CompanyBranchDto.class));
    }

    @Test
    void updateDelegatesUpdateToService() throws Exception {
        final CompanyBranchDto companyBranchDto = newCompanyBranchDto();
        mockMvc.perform(
                put("/company-branch")
                        .content(objectMapper.writeValueAsString(companyBranchDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(companyBranchService).update(any(CompanyBranchDto.class));
    }

    @Test
    void deleteDelegatesDeleteToService() throws Exception {
        final long id = 1L;
        mockMvc.perform(
                delete("/company-branch")
                        .param("id", String.valueOf(id)))
                .andExpect(status().is2xxSuccessful());
        verify(companyBranchService).delete(eq(id));
    }

    private CompanyBranchDto newCompanyBranchDto() {
        final CompanyDto companyDto = CompanyDto.builder()
                .setId(2L)
                .build();

        return CompanyBranchDto.builder()
                .setId(1L)
                .setName("Name")
                .setCompany(companyDto)
                .build();
    }

}
