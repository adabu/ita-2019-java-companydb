package com.ita2019java.usersapp.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = CompanyBranchDto.CompanyBranchDtoBuilder.class)
public class CompanyBranchDto extends AbstractDto {

    private final String name;
    private final AddressDto location;
    private final CompanyDto company;
    private final List<EmployeeDto> employees;

    private CompanyBranchDto(Long id, LocalDateTime createdAt, LocalDateTime updatedAt,
                             String name, AddressDto location,
                             CompanyDto company, List<EmployeeDto> employees) {
        super(id, createdAt, updatedAt);
        this.name = name;
        this.location = location;
        this.company = company;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public AddressDto getLocation() {
        return location;
    }

    public CompanyDto getCompany() {
        return company;
    }

    public List<EmployeeDto> getEmployees() {
        return employees;
    }

    public static CompanyBranchDtoBuilder builder() {
        return new CompanyBranchDtoBuilder();
    }

    @Override
    public String toString() {
        return getName() + "(" + company.getName() + ")";
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class CompanyBranchDtoBuilder {

        private Long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String name;
        private AddressDto location;
        private CompanyDto company;
        private List<EmployeeDto> employees;

        public CompanyBranchDtoBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public CompanyBranchDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public CompanyBranchDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public CompanyBranchDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CompanyBranchDtoBuilder setLocation(AddressDto location) {
            this.location = location;
            return this;
        }

        public CompanyBranchDtoBuilder setCompany(CompanyDto company) {
            this.company = company;
            return this;
        }

        public CompanyBranchDtoBuilder setEmployees(List<EmployeeDto> employees) {
            this.employees = employees;
            return this;
        }

        public CompanyBranchDto build() {
            return new CompanyBranchDto(
                    id, createdAt, updatedAt,
                    name, location,
                    company, employees
            );
        }

    }

}
