package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.entity.Project;

/**
 * Contains methods for transformation between instances of {@code ProjectDto} and {@code Project}
 * and lists of their instances.
 */
public interface ProjectTransformer extends HeavyTransformer<ProjectDto, Project> {
}
