package com.ita2019java.usersapp.config;

import com.ita2019java.usersapp.ares.util.CompanyExtractor;
import com.ita2019java.usersapp.dto.mapping.*;
import com.ita2019java.usersapp.repository.CompanyRepository;
import com.ita2019java.usersapp.service.AresService;
import com.ita2019java.usersapp.service.CompanyService;
import com.ita2019java.usersapp.service.CompanyServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class CompanyConfig {

    @Bean
    public CompanyService companyService(CompanyRepository companyRepository, CompanyTransformer companyTransformer,
                                         AresService aresService, CompanyExtractor companyExtractor) {
        return new CompanyServiceImpl(companyRepository, companyTransformer, aresService, companyExtractor);
    }

    @Bean
    public CompanyTransformer companyTransformer(AddressTransformer addressTransformer,
                                                 @Lazy CompanyBranchTransformer companyBranchTransformer,
                                                 @Lazy ProjectTransformer projectTransformer) {
        return new CompanyTransformerImpl(addressTransformer, companyBranchTransformer, projectTransformer);
    }

}
