package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.repository.util.SchemaUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class EmployeeRepositoryIT {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SchemaUtils schemaUtils;

    @BeforeAll
    void beforeAll() {
        // re-create test schema
        schemaUtils.resetSchema();
    }

    @AfterAll
    void afterAll() {
        // drop test schema
        schemaUtils.cleanSchema();
    }

    @Test
    @Transactional(readOnly = true)
    void findAllReturnsAll() {
        // companies count may be affected by other tests...
        schemaUtils.resetSchema();

        final long companyId = 1L;
        List<Employee> employeesOfCompany = employeeRepository.findAll(companyId);
        final int employeesCount = 4;
        assertThat(employeesOfCompany).hasSize(employeesCount);
    }

    @Test
    void createCreatesEmployee() {
        final Employee employee = createEmployee();

        final long companyId = getCompanyId(employee);
        List<Employee> employeesOfCompany = employeeRepository.findAll(companyId);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("firstname").doesNotContain(employee);

        employeeRepository.create(employee);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assertThat(employeesOfCompany).usingElementComparatorOnFields("firstname").contains(employee);
    }

    @Test
    void updateUpdatesEmployee() {
        Employee employee = createEmployee();
        employeeRepository.create(employee);
        final long id = employee.getId();

        final long companyId = getCompanyId(employee);
        List<Employee> employeesOfCompany = employeeRepository.findAll(companyId);

        employee = createEmployee();
        employee.setSurname("Otherdoe");
        employee.setId(id);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("surname").doesNotContain(employee);

        employeeRepository.update(employee);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assertThat(employeesOfCompany).usingElementComparatorOnFields("surname").contains(employee);
    }

    @Test
    void deleteDeletesEmployee() {
        final Employee employee = createEmployee();
        employee.setFirstname("George");

        final long companyId = getCompanyId(employee);
        List<Employee> employeesOfCompany = employeeRepository.findAll(companyId);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("firstname").doesNotContain(employee);

        employeeRepository.create(employee);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("firstname").contains(employee);

        employeeRepository.delete(employee);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assertThat(employeesOfCompany).usingElementComparatorOnFields("firstname").doesNotContain(employee);
    }

    @Test
    void deleteByIdDeletedEmployee() {
        final Employee employee = createEmployee();
        employee.setFirstname("Johnathan");

        final long companyId = getCompanyId(employee);
        List<Employee> employeesOfCompany = employeeRepository.findAll(companyId);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("firstname").doesNotContain(employee);

        employeeRepository.create(employee);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assumeThat(employeesOfCompany).usingElementComparatorOnFields("firstname").contains(employee);

        final Long id = employee.getId();
        employeeRepository.deleteById(id);
        employeesOfCompany = employeeRepository.findAll(companyId);
        assertThat(employeesOfCompany).usingElementComparatorOnFields("firstname").doesNotContain(employee);
    }

    private Employee createEmployee() {
        CompanyBranch companyBranch = new CompanyBranch();
        companyBranch.setId(1L);

        final Company company = new Company();
        company.setId(1L);
        companyBranch.setCompany(company);

        final Employee employee = new Employee();
        employee.setFirstname("John");
        employee.setSurname("Doe");
        employee.setCompanyBranch(companyBranch);

        return employee;
    }

    private Long getCompanyId(Employee employee) {
        return employee.getCompanyBranch().getCompany().getId();
    }

}
