package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.dto.mapping.util.Checker;
import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Project;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * If the DTO/Entity object to be transformed contains reference to other DTO/Entity object,
 * the resulting Entity/DTO object contains reference to according Entity/DTO object, but only its id is set.
 */
public class CompanyTransformerImpl implements CompanyTransformer {

    private final AddressTransformer addressTransformer;
    private final CompanyBranchTransformer companyBranchTransformer;
    private final ProjectTransformer projectTransformer;

    public CompanyTransformerImpl(AddressTransformer addressTransformer,
                                  CompanyBranchTransformer companyBranchTransformer,
                                  ProjectTransformer projectTransformer) {
        this.addressTransformer = addressTransformer;
        this.companyBranchTransformer = companyBranchTransformer;
        this.projectTransformer = projectTransformer;
    }

    @Override
    public CompanyDto toDto(Company company) {
        AddressDto addressDto = null;
        Address address = company.getHeadquarters();
        if (address != null) {
            addressDto = addressTransformer.toDto(address);
        }

        List<CompanyBranchDto> companyBranchDtos = Collections.emptyList();
        List<CompanyBranch> companyBranches = company.getCompanyBranches();
        if (companyBranches != null) {
            if (Checker.containsNull(companyBranches)) {
                throw new IllegalArgumentException("Company branches of company contain null element.");
            }
            companyBranchDtos = companyBranchTransformer.toDtosWithId(companyBranches);
        }

        List<ProjectDto> projectDtos = Collections.emptyList();
        List<Project> projects = company.getProjects();
        if (projects != null) {
            if (Checker.containsNull(projects)) {
                throw new IllegalArgumentException("Projects of company contain null element.");
            }
            projectDtos = projectTransformer.toDtosWithId(projects);
        }

        return CompanyDto.builder()
                .setId(company.getId())
                .setCreatedAt(company.getCreatedAt())
                .setUpdatedAt(company.getUpdatedAt())
                .setName(company.getName())
                .setHeadquarters(addressDto)
                .setCompanyId(company.getCompanyId())
                .setVatId(company.getVatId())
                .setWebUrl(company.getWebUrl())
                .setNote(company.getNote())
                .setActive(company.isActive())
                .setCompanyBranches(companyBranchDtos)
                .setProjects(projectDtos)
                .build();
    }

    @Override
    public Company toEntity(CompanyDto companyDto) {
        Company company = new Company();

        company.setId(companyDto.getId());
        company.setCreatedAt(companyDto.getCreatedAt());
        company.setUpdatedAt(companyDto.getUpdatedAt());
        company.setName(companyDto.getName());

        Address address = null;
        AddressDto addressDto = companyDto.getHeadquarters();
        if (addressDto != null) {
            address = addressTransformer.toEntity(addressDto);
        }
        company.setHeadquarters(address);

        company.setCompanyId(companyDto.getCompanyId());
        company.setVatId(companyDto.getVatId());
        company.setWebUrl(companyDto.getWebUrl());
        company.setNote(companyDto.getNote());
        company.setActive(companyDto.isActive());

        List<CompanyBranch> companyBranches = Collections.emptyList();
        List<CompanyBranchDto> companyBranchDtos = companyDto.getCompanyBranches();
        if (companyBranchDtos != null) {
            if (Checker.containsNull(companyBranchDtos)) {
                throw new IllegalArgumentException("Company branches of company DTO contain null element.");
            }
            companyBranches = companyBranchTransformer.toEntitiesWithId(companyBranchDtos);
        }
        company.setCompanyBranches(companyBranches);

        List<Project> projects = Collections.emptyList();
        List<ProjectDto> projectDtos = companyDto.getProjects();
        if (projectDtos != null) {
            if (Checker.containsNull(projectDtos)) {
                throw new IllegalArgumentException("Projects of company DTO contain null element.");
            }
            projects = projectTransformer.toEntitiesWithId(projectDtos);
        }
        company.setProjects(projects);

        return company;
    }

    @Override
    public CompanyDto toDtoWithId(Company company) {
        return CompanyDto.builder()
                .setId(company.getId())
                .build();
    }

    @Override
    public Company toEntityWithId(CompanyDto companyDto) {
        Company company = new Company();
        company.setId(companyDto.getId());
        return company;
    }

    @Override
    public List<CompanyDto> toDtosWithId(List<Company> companies) {
        return companies.stream()
                .map(this::toDtoWithId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Company> toEntitiesWithId(List<CompanyDto> companyDtos) {
        return companyDtos.stream()
                .map(this::toEntityWithId)
                .collect(Collectors.toList());
    }

}
