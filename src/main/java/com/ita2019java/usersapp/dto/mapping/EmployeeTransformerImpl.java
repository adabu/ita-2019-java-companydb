package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.dto.mapping.util.Checker;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.entity.Project;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * If the DTO/Entity object to be transformed contains reference to other DTO/Entity object,
 * the resulting Entity/DTO object contains reference to according Entity/DTO object, but only its id is set.
 */
public class EmployeeTransformerImpl implements EmployeeTransformer {

    private final CompanyBranchTransformer companyBranchTransformer;
    private final ProjectTransformer projectTransformer;

    public EmployeeTransformerImpl(CompanyBranchTransformer companyBranchTransformer,
                                   ProjectTransformer projectTransformer) {
        this.companyBranchTransformer = companyBranchTransformer;
        this.projectTransformer = projectTransformer;
    }

    @Override
    public EmployeeDto toDto(Employee employee) {
        CompanyBranchDto companyBranchDto = null;
        CompanyBranch companyBranch = employee.getCompanyBranch();
        if (companyBranch != null) {
            companyBranchDto = companyBranchTransformer.toDtoWithId(companyBranch);
        }

        List<ProjectDto> projectDtos = Collections.emptyList();
        List<Project> projects = employee.getProjects();
        if (projects != null) {
            if (Checker.containsNull(projects)) {
                throw new IllegalArgumentException("Projects of employee contain null element.");
            }
            projectDtos = projectTransformer.toDtosWithId(projects);
        }

        return EmployeeDto.builder()
                .setId(employee.getId())
                .setCreatedAt(employee.getCreatedAt())
                .setUpdatedAt(employee.getUpdatedAt())
                .setFirstname(employee.getFirstname())
                .setSurname(employee.getSurname())
                .setSalary(employee.getSalary())
                .setCompanyBranch(companyBranchDto)
                .setProjects(projectDtos)
                .build();
    }

    @Override
    public Employee toEntity(EmployeeDto employeeDto) {
        Employee employee = new Employee();

        employee.setId(employeeDto.getId());
        employee.setCreatedAt(employeeDto.getCreatedAt());
        employee.setUpdatedAt(employeeDto.getUpdatedAt());
        employee.setFirstname(employeeDto.getFirstname());
        employee.setSurname(employeeDto.getSurname());
        employee.setSalary(employeeDto.getSalary());

        CompanyBranch companyBranch = null;
        CompanyBranchDto companyBranchDto = employeeDto.getCompanyBranch();
        if (companyBranchDto != null) {
            companyBranch = companyBranchTransformer.toEntityWithId(companyBranchDto);
        }
        employee.setCompanyBranch(companyBranch);

        List<Project> projects = Collections.emptyList();
        List<ProjectDto> projectDtos = employeeDto.getProjects();
        if (projectDtos != null) {
            if (Checker.containsNull(projectDtos)) {
                throw new IllegalArgumentException("Projects of employee DTO contain null element.");
            }
            projects = projectTransformer.toEntitiesWithId(projectDtos);
        }
        employee.setProjects(projects);

        return employee;
    }

    @Override
    public EmployeeDto toDtoWithId(Employee employee) {
        return EmployeeDto.builder()
                .setId(employee.getId())
                .build();
    }

    @Override
    public Employee toEntityWithId(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setId(employeeDto.getId());
        return employee;
    }

    @Override
    public List<EmployeeDto> toDtosWithId(List<Employee> employees) {
        return employees.stream()
                .map(this::toDtoWithId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Employee> toEntitiesWithId(List<EmployeeDto> employeeDtos) {
        return employeeDtos.stream()
                .map(this::toEntityWithId)
                .collect(Collectors.toList());
    }

}
