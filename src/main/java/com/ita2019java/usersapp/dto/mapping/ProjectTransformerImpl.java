package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.dto.mapping.util.Checker;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.Employee;
import com.ita2019java.usersapp.entity.Project;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * If the DTO/Entity object to be transformed contains reference to other DTO/Entity object,
 * the resulting Entity/DTO object contains reference to according Entity/DTO object, but only its id is set.
 */
public class ProjectTransformerImpl implements ProjectTransformer {

    private final CompanyTransformer companyTransformer;
    private final EmployeeTransformer employeeTransformer;

    public ProjectTransformerImpl(CompanyTransformer companyTransformer, EmployeeTransformer employeeTransformer) {
        this.companyTransformer = companyTransformer;
        this.employeeTransformer = employeeTransformer;
    }

    @Override
    public ProjectDto toDto(Project project) {
        CompanyDto companyDto = null;
        Company company = project.getCompany();
        if (company != null) {
            companyDto = companyTransformer.toDtoWithId(company);
        }

        List<EmployeeDto> employeeDtos = Collections.emptyList();
        List<Employee> employees = project.getEmployees();
        if (employees != null) {
            if (Checker.containsNull(employees)) {
                throw new IllegalArgumentException("Employees of project contain null element.");
            }
            employeeDtos = employeeTransformer.toDtosWithId(employees);
        }

        return ProjectDto.builder()
                .setId(project.getId())
                .setCreatedAt(project.getCreatedAt())
                .setUpdatedAt(project.getUpdatedAt())
                .setName(project.getName())
                .setStartDate(project.getStartDate())
                .setEndDate(project.getEndDate())
                .setPrice(project.getPrice())
                .setCompany(companyDto)
                .setEmployees(employeeDtos)
                .build();
    }

    @Override
    public Project toEntity(ProjectDto projectDto) {
        Project project = new Project();

        project.setId(projectDto.getId());
        project.setCreatedAt(projectDto.getCreatedAt());
        project.setUpdatedAt(projectDto.getUpdatedAt());
        project.setName(projectDto.getName());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setPrice(projectDto.getPrice());

        Company company = null;
        CompanyDto companyDto = projectDto.getCompany();
        if (companyDto != null) {
            company = companyTransformer.toEntityWithId(companyDto);
        }
        project.setCompany(company);

        List<Employee> employees = Collections.emptyList();
        List<EmployeeDto> employeeDtos = projectDto.getEmployees();
        if (employeeDtos != null) {
            if (Checker.containsNull(employeeDtos)) {
                throw new IllegalArgumentException("Employees of project DTO contain null element.");
            }
            employees = employeeTransformer.toEntitiesWithId(employeeDtos);
        }
        project.setEmployees(employees);

        return project;
    }

    @Override
    public ProjectDto toDtoWithId(Project project) {
        return ProjectDto.builder()
                .setId(project.getId())
                .build();
    }

    @Override
    public Project toEntityWithId(ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.getId());
        return project;
    }

    @Override
    public List<ProjectDto> toDtosWithId(List<Project> projects) {
        return projects.stream()
                .map(this::toDtoWithId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> toEntitiesWithId(List<ProjectDto> projectDtos) {
        return projectDtos.stream()
                .map(this::toEntityWithId)
                .collect(Collectors.toList());
    }

}
