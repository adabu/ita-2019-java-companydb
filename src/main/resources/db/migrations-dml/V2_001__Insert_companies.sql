INSERT INTO company (created_at, updated_at, active, company_id,
                     address_city, address_country, address_house_number, address_street, address_zip_code,
                     name, note, vat_id, web_url)
VALUES (now(), now(), true, 'COMP00ID',
        'Praha', 'CZE', '111/42', 'Pražská', '700 80',
        'Abeceda s.r.o.', 'Písmenka všeho druhu!', 'VAT12345ID', null),
       (now(), now(), false, '2NDCOMP0',
        'Žilina', 'SVK', '318/3', 'Nová', '512 04',
        'Techno', 'Průmyslová technika', '2NDVAT00', null);
