package com.ita2019java.usersapp.dto.mapping;

/**
 * Contains methods for transformation between data transfer object and entity instances.
 * @param <Dto> the DTO (data transfer object) type
 * @param <Entity> the entity type
 */
public interface IdTransformer<Dto, Entity> {

    /**
     * Returns DTO with only id property set in accordance with {@code entity}.
     * @param entity the entity to transform
     * @return the transformed DTO
     */
    Dto toDtoWithId(Entity entity);

    /**
     * Returns entity with only id property set in accordance with {@code dto}.
     * @param dto the DTO to transform
     * @return the transformed entity
     */
    Entity toEntityWithId(Dto dto);

}
