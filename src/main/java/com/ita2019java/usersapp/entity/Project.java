package com.ita2019java.usersapp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "project")
public class Project extends AbstractEntity {

    @Column(name = "name", length = 100, nullable = false)
    private String name;
    @Column(name = "start_date", length = 20)
    private String startDate;
    @Column(name = "end_date")
    private String endDate;
    @Column(name = "price")
    private Long price;
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "project_company_fk"))
    private Company company;
    @ManyToMany
    @JoinTable(
            name = "project_employee",
            joinColumns = @JoinColumn(
                    name = "project_id",
                    foreignKey = @ForeignKey(name = "project_employee_project_fk")
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "employee_id",
                    foreignKey = @ForeignKey(name = "project_employee_employee_fk")
            )
    )
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return getName() + " (" + getCompany().getName() + ")";
    }

}
