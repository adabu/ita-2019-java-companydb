package com.ita2019java.usersapp.repository.util;

import org.flywaydb.core.Flyway;

public class SchemaUtils {

    private final Flyway flyway;

    public SchemaUtils(Flyway flyway) {
        this.flyway = flyway;
    }

    public void resetSchema() {
        cleanSchema();
        migrateSchema();
    }

    public void cleanSchema() {
        flyway.clean();
    }

    public void migrateSchema() {
        flyway.migrate();
    }

}
