CREATE TABLE IF NOT EXISTS company
(
    id         BIGINT PRIMARY KEY,
    name       VARCHAR(100) NOT NULL,
    vat_id     VARCHAR(10),
    company_id VARCHAR(8),
    web_url    VARCHAR(100),
    note       VARCHAR(255),
    active     BOOLEAN      NOT NULL
);