package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

public class ProjectRepositoryImpl implements ProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @Override
    public List<Project> findAll(String name, LocalDateTime createdAfter) {
        return entityManager.createQuery("SELECT p FROM Project p " +
                        "WHERE p.name LIKE :nameSubstring AND p.createdAt > :createdAfter",
                Project.class)
                .setParameter("nameSubstring", "%" + name + "%")
                .setParameter("createdAfter", createdAfter)
                .getResultList();
    }

    @Override
    public void create(Project project) {
        entityManager.persist(project);
    }

    @Override
    public void update(Project project) {
        entityManager.merge(project);
    }

    @Override
    public void delete(Project project) {
        entityManager.remove(project);
    }

    @Override
    public void deleteById(Long id) {
        Project project = entityManager.find(Project.class, id);
        entityManager.remove(project);
    }

}
