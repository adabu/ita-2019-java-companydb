package com.ita2019java.usersapp.dto.mapping;

/**
 * Contains methods for full transformation between data transfer object and entity instances,
 * for transformation between data transfer object and entity instances with only set id and
 * for transformation between list of data transfer object instances and list of entity instances with only set id.
 * @param <Dto> the DTO (data transfer object) type
 * @param <Entity> the entity type
 */
public interface HeavyTransformer<Dto, Entity> extends
        Transformer<Dto, Entity>,
        IdTransformer<Dto, Entity>,
        ListIdTransformer<Dto, Entity> {
}
