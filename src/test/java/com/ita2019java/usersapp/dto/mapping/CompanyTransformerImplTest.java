package com.ita2019java.usersapp.dto.mapping;

import com.ita2019java.usersapp.dto.AddressDto;
import com.ita2019java.usersapp.dto.CompanyBranchDto;
import com.ita2019java.usersapp.dto.CompanyDto;
import com.ita2019java.usersapp.dto.ProjectDto;
import com.ita2019java.usersapp.entity.Address;
import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.CompanyBranch;
import com.ita2019java.usersapp.entity.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class CompanyTransformerImplTest {

    private CompanyTransformer companyTransformer;
    @Mock
    private AddressTransformer addressTransformer;
    @Mock
    private CompanyBranchTransformer companyBranchTransformer;
    @Mock
    private ProjectTransformer projectTransformer;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);

        companyTransformer = new CompanyTransformerImpl(addressTransformer,
                companyBranchTransformer, projectTransformer);
    }

    @Test
    void transformationFromEntityToDtoReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final Address headquarters = new Address(null, null, null, null, null);
        final String companyId = "Some company id";
        final String vatId = "Some vat id";
        final String webUrl = "Some web url";
        final String note = "Some note";
        final boolean active = true;
        final List<CompanyBranch> companyBranches = List.of(new CompanyBranch());
        final List<Project> projects = List.of(new Project());

        Company company = new Company();
        company.setId(id);
        company.setCreatedAt(createdAt);
        company.setUpdatedAt(updatedAt);
        company.setName(name);
        company.setHeadquarters(headquarters);
        company.setCompanyId(companyId);
        company.setVatId(vatId);
        company.setWebUrl(webUrl);
        company.setNote(note);
        company.setActive(active);
        company.setCompanyBranches(companyBranches);
        company.setProjects(projects);

        AddressDto headquartersDto =
                new AddressDto(null, null, null, null, null);
        when(addressTransformer.toDto(headquarters)).thenReturn(headquartersDto);

        List<CompanyBranchDto> companyBranchDtos = List.of(CompanyBranchDto.builder().build());
        when(companyBranchTransformer.toDtosWithId(companyBranches)).thenReturn(companyBranchDtos);

        List<ProjectDto> projectDtos = List.of(ProjectDto.builder().build());
        when(projectTransformer.toDtosWithId(projects)).thenReturn(projectDtos);

        CompanyDto companyDto = companyTransformer.toDto(company);

        assertThat(companyDto.getId()).isEqualTo(id);
        assertThat(companyDto.getCreatedAt()).isEqualTo(createdAt);
        assertThat(companyDto.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(companyDto.getName()).isEqualTo(name);
        assertThat(companyDto.getHeadquarters()).isSameAs(headquartersDto);
        assertThat(companyDto.getCompanyId()).isEqualTo(companyId);
        assertThat(companyDto.getVatId()).isEqualTo(vatId);
        assertThat(companyDto.getWebUrl()).isEqualTo(webUrl);
        assertThat(companyDto.getNote()).isEqualTo(note);
        assertThat(companyDto.isActive()).isEqualTo(active);
        assertThat(companyDto.getCompanyBranches()).isSameAs(companyBranchDtos);
        assertThat(companyDto.getProjects()).isSameAs(projectDtos);
    }

    @Test
    void transformationFromEntityToDtoWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        Company company = new Company();
        company.setId(null);
        company.setCreatedAt(null);
        company.setUpdatedAt(null);
        company.setName(null);
        company.setHeadquarters(null);
        company.setCompanyId(null);
        company.setVatId(null);
        company.setWebUrl(null);
        company.setNote(null);
        company.setCompanyBranches(null);
        company.setProjects(null);

        CompanyDto companyDto = companyTransformer.toDto(company);

        final Object[] companyDtoPropertyValues = new Object[]{companyDto.getId(), companyDto.getCreatedAt(),
                companyDto.getUpdatedAt(), companyDto.getName(), companyDto.getHeadquarters(),
                companyDto.getCompanyId(), companyDto.getVatId(), companyDto.getWebUrl(), companyDto.getNote()};
        assertThat(companyDtoPropertyValues).containsOnlyNulls();
        assertThat(companyDto.getCompanyBranches()).isEmpty();
        assertThat(companyDto.getProjects()).isEmpty();
    }

    @Test
    void transformationFromDtoToEntityReturnsCorrectDto() {
        final Long id = 10L;
        final LocalDateTime createdAt = LocalDateTime.MIN;
        final LocalDateTime updatedAt = LocalDateTime.MAX;
        final String name = "Some name";
        final AddressDto headquartersDto =
                new AddressDto(null, null, null, null, null);
        final String companyId = "Some company id";
        final String vatId = "Some vat id";
        final String webUrl = "Some web url";
        final String note = "Some note";
        final boolean active = true;
        final List<CompanyBranchDto> companyBranchDtos = List.of(CompanyBranchDto.builder().build());
        final List<ProjectDto> projectDtos = List.of(ProjectDto.builder().build());

        CompanyDto companyDto = CompanyDto.builder()
                .setId(id)
                .setCreatedAt(createdAt)
                .setUpdatedAt(updatedAt)
                .setName(name)
                .setHeadquarters(headquartersDto)
                .setCompanyId(companyId)
                .setVatId(vatId)
                .setWebUrl(webUrl)
                .setNote(note)
                .setActive(active)
                .setCompanyBranches(companyBranchDtos)
                .setProjects(projectDtos)
                .build();

        Address headquarters =
                new Address(null, null, null, null, null);
        when(addressTransformer.toEntity(headquartersDto)).thenReturn(headquarters);

        List<CompanyBranch> companyBranches = List.of(new CompanyBranch());
        when(companyBranchTransformer.toEntitiesWithId(companyBranchDtos)).thenReturn(companyBranches);

        List<Project> projects = List.of(new Project());
        when(projectTransformer.toEntitiesWithId(projectDtos)).thenReturn(projects);

        Company company = companyTransformer.toEntity(companyDto);

        assertThat(company.getId()).isEqualTo(id);
        assertThat(company.getCreatedAt()).isEqualTo(createdAt);
        assertThat(company.getUpdatedAt()).isEqualTo(updatedAt);
        assertThat(company.getName()).isEqualTo(name);
        assertThat(company.getHeadquarters()).isSameAs(headquarters);
        assertThat(company.getCompanyId()).isEqualTo(companyId);
        assertThat(company.getVatId()).isEqualTo(vatId);
        assertThat(company.getWebUrl()).isEqualTo(webUrl);
        assertThat(company.getNote()).isEqualTo(note);
        assertThat(company.isActive()).isEqualTo(active);
        assertThat(company.getCompanyBranches()).isSameAs(companyBranches);
        assertThat(company.getProjects()).isSameAs(projects);
    }

    @Test
    void transformationFromDtoToEntityWithNullValuesReturnsDtoWithNullsAndEmptyCollections() {
        CompanyDto companyDto = CompanyDto.builder()
                .setId(null)
                .setCreatedAt(null)
                .setUpdatedAt(null)
                .setName(null)
                .setHeadquarters(null)
                .setCompanyId(null)
                .setVatId(null)
                .setWebUrl(null)
                .setNote(null)
                .setCompanyBranches(null)
                .setProjects(null)
                .build();

        Company company = companyTransformer.toEntity(companyDto);

        final Object[] companyPropertyValues = new Object[]{company.getId(), company.getCreatedAt(),
                company.getUpdatedAt(), company.getName(), company.getHeadquarters(),
                company.getCompanyId(), company.getVatId(), company.getWebUrl(), company.getNote()};
        assertThat(companyPropertyValues).containsOnlyNulls();
        assertThat(company.getCompanyBranches()).isEmpty();
        assertThat(company.getProjects()).isEmpty();
    }

}
