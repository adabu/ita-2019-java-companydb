package com.ita2019java.usersapp.controller;

import com.ita2019java.usersapp.dto.EmployeeDto;
import com.ita2019java.usersapp.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeDto> getEmployees(@RequestParam("companyId") long companyId) {
        LOGGER.info("Get employees of specific company called.");
        List<EmployeeDto> employeeDtos = new ArrayList<>(employeeService.findAll(companyId));
        LOGGER.info("Returning {} employees.", employeeDtos.size());
        return employeeDtos;
    }

    @PostMapping
    public void createEmployee(@RequestBody EmployeeDto employee) {
        LOGGER.info("Create employee {} called.", employee);
        employeeService.create(employee);
        LOGGER.info("Employee {} was created.", employee);
    }

    @PutMapping
    public void updateCompany(@RequestBody EmployeeDto employee) {
        LOGGER.info("Update employee {} called.", employee);
        employeeService.update(employee);
        LOGGER.info("Employee {} was updated.", employee);
    }

    @DeleteMapping
    public void deleteCompany(@RequestParam("id") long employeeId) {
        LOGGER.info("Delete employee of id {} called.", employeeId);
        employeeService.delete(employeeId);
        LOGGER.info("Employee of id {} was deleted.", employeeId);
    }

}
