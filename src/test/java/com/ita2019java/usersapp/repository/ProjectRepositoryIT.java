package com.ita2019java.usersapp.repository;

import com.ita2019java.usersapp.entity.Company;
import com.ita2019java.usersapp.entity.Project;
import com.ita2019java.usersapp.repository.util.SchemaUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class ProjectRepositoryIT {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private SchemaUtils schemaUtils;

    @BeforeAll
    void beforeAll() {
        // re-create test schema
        schemaUtils.resetSchema();
    }

    @AfterAll
    void afterAll() {
        // drop test schema
        schemaUtils.cleanSchema();
    }

    @Test
    @Transactional(readOnly = true)
    void findAllReturnsAll() {
        // companies count may be affected by other tests...
        schemaUtils.resetSchema();

        final int insertedProjectsCount = 3;
        List<Project> returnedProjects = projectRepository.findAll();
        assertThat(returnedProjects).hasSize(insertedProjectsCount);
    }

    @Test
    @Transactional(readOnly = true)
    void findAllWithNameAndCreationTimestampReturnsAll() {
        // companies count may be affected by other tests...
        schemaUtils.resetSchema();

        final String nameSubstring = "Louka";
        LocalDateTime creationLimitDateTime = LocalDate.of(2010, Month.JANUARY, 1).atStartOfDay();
        List<Project> returnedProjects = projectRepository.findAll("Louka", creationLimitDateTime);
        int filteredProjectsCount = 1;
        assertThat(returnedProjects).hasSize(filteredProjectsCount);

        final String substringContainedInAllNames = "k";
        returnedProjects = projectRepository.findAll(substringContainedInAllNames, creationLimitDateTime);
        filteredProjectsCount = 3;
        assertThat(returnedProjects).hasSize(filteredProjectsCount);

        creationLimitDateTime = LocalDate.of(2030, Month.JANUARY, 1).atStartOfDay();
        returnedProjects = projectRepository.findAll("Louka", creationLimitDateTime);
        filteredProjectsCount = 0;
        assertThat(returnedProjects).hasSize(filteredProjectsCount);
    }

    @Test
    void createCreatesProject() {
        final Project project = createProject();
        projectRepository.create(project);
        assumeThat(project.getId()).isNotNull();

        List<Project> storedProjects = projectRepository.findAll();
        assertThat(storedProjects).usingElementComparatorOnFields("id").contains(project);
    }

    @Test
    void updateUpdatesEmployee() {
        Project project = createProject();
        projectRepository.create(project);
        assumeThat(project.getId()).isNotNull();
        final long id = project.getId();

        final String newName = "Nonexistent project name";
        project = createProject();
        project.setName(newName);
        List<Project> storedProjects = projectRepository.findAll();
        assumeThat(storedProjects).usingElementComparatorOnFields("name").doesNotContain(project);

        project.setId(id);
        projectRepository.update(project);
        assertThat(storedProjects).usingElementComparatorOnFields("id").contains(project);
        assumeThat(storedProjects).usingElementComparatorOnFields("name").contains(project);
    }

    @Test
    void deleteDeletesProject() {
        final Project project = createProject();
        projectRepository.create(project);

        projectRepository.delete(project);
        List<Project> storedProjects = projectRepository.findAll();
        assertThat(storedProjects).usingElementComparatorOnFields("id").doesNotContain(project);
    }

    @Test
    void deleteByIdDeletedEmployee() {
        final Project project = createProject();
        projectRepository.create(project);

        projectRepository.deleteById(project.getId());
        List<Project> storedProjects = projectRepository.findAll();
        assertThat(storedProjects).usingElementComparatorOnFields("id").doesNotContain(project);
    }

    private Project createProject() {
        final Company company = new Company();
        company.setId(1L);

        final Project project = new Project();
        project.setName("Name");
        project.setCompany(company);

        return project;
    }

}
